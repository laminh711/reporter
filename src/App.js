import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import "./App.scss";
import AppRouter from "./scripts/AppRouter";
import { ThemeProvider } from "@material-ui/core";
import blueeeeeTheme from "./scripts/configs/Theme";

function App() {
    return (
        <Router>
            <ThemeProvider theme={blueeeeeTheme}>
                <AppRouter />
            </ThemeProvider>
        </Router>
    );
}

export default App;
