import "babel-polyfill";
import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import App from "./App";
import "./index.css";
import { idb__setDefaultAppSetting } from "./scripts/indexdb/idb";
import { setSettings } from "./scripts/redux/actionTypes/settingActionTypes";
import { dispatchSetDefaultValue, reduxStore } from "./scripts/redux/reduxStore";
import * as serviceWorker from "./serviceWorker";
import { setTriggerTime } from "./scripts/redux/actions/appActions";

dispatchSetDefaultValue();

// In the following line, you should include the prefixes
// of implementations you want to test.
// window.indexedDB = window.indexedDB || window.mozIndexedDB || window.webkitIndexedDB || window.msIndexedDB;
// DON'T use "var indexedDB = ..." if you're not in a function.
// Moreover, you may need references to some window.IDB* objects:
window.IDBTransaction =
    window.IDBTransaction || window.webkitIDBTransaction || window.msIDBTransaction;
window.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange || window.msIDBKeyRange;
// (Mozilla has never prefixed these objects, so we don't
//  need window.mozIDB*)
if (!window.indexedDB) {
    console.log(
        "Your browser doesn't support a stable version of IndexedDB. Such and such feature will not be available."
    );
} else {
    idb__setDefaultAppSetting().then((setting) => {
        reduxStore.dispatch(
            setSettings({
                workDuration: setting.work_duration,
                breakDuration: setting.break_duration,
                longBreakDuration: setting.long_break_duration,
                isNotificationOn: setting.is_notification_on,
                isSoundOn: setting.is_sound_on,
            })
        );
        reduxStore.dispatch(
            setTriggerTime({
                triggerDuration: setting.work_duration,
            })
        );
    });
}

ReactDOM.render(
    <Provider store={reduxStore}>
        <App />
    </Provider>,
    document.getElementById("root")
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.register();
