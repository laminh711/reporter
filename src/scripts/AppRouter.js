import { Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, IconButton, Snackbar } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React, { Component } from "react";
import { Helmet } from "react-helmet";
import { connect } from "react-redux";
import { Redirect, Route, Switch, withRouter } from "react-router-dom";
import finishIntervalSound from "../assets/sounds/zapsplat_bell_service_desk_press_many_fast_frustrated_angry_18036.mp3";
import AppDrawer from "./components/AppDrawer";
import ReportFormModal from "./components/ReportFormModal";
import { AP, APList } from "./configs/AppPages";
import AppTop from "./layouts/AppTop";
import BottomNavigtion from "./layouts/BottomNavigtion";
import { createNotification, requestNotificationPermission } from "./notifications";
import SimplePage from "./pages/SimplePage";
import { setRedirect } from "./redux/actions/appActions";
import { closeMessage, popMessage } from "./redux/actions/snackbarAction";
import { BreakTypeInterval, FinishingInterval, LongBreakTypeInterval, RunningInterval, WorkTypeInterval } from "./redux/reducers/appReducer";

class AppRouter extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isNotificationAskingOpen: false,
            isReportModalOpen: false,
            isMessageSnackbarOpen: false,
        };

        this.renderBottomNavigation = this.renderBottomNavigation.bind(this);
    }

    setIsReportModalOpen = (isOpen) => {
        this.setState({
            isReportModalOpen: isOpen,
        });
    };

    setIsNotificationAskingOpen = (isOpen) => {
        this.setState({
            isNotificationAskingOpen: isOpen,
        });
    };

    setIsMessageSnackbarOpen = (isOpen) => {
        this.setState({ isMessageSnackbarOpen: isOpen });
    };

    onCloseMessageSnackbar = (e) => {
        this.setIsMessageSnackbarOpen(false);
    };

    componentDidMount() {
        if (Notification.permission !== "granted") {
            // this.setIsMessageSnackbarOpen(true);
            this.props.popMessage({
                message: "Notification is off, turn it on in settings.",
            });
        }
    }

    renderBottomNavigation() {
        return <BottomNavigtion />;
    }

    renderAppTop() {
        return <AppTop />;
    }

    componentDidUpdate(prevProps, prevState) {
        if (prevProps.redirect === "" && this.props.redirect !== "") {
            this.props.setRedirect("");
        }

        if (
            prevProps.triggerState === RunningInterval &&
            this.props.triggerState === FinishingInterval
        ) {
            if (this.props.isSoundOn) {
                document.getElementById("myAudio").play();
            }
            if (this.props.isNotificationOn) {
                switch (this.props.triggerType) {
                    case WorkTypeInterval:
                        createNotification(
                            "The work interval is finished, report your work if you can and take some breaks!"
                        );
                        break;
                    case BreakTypeInterval:
                        createNotification("Break's over, back to work!");
                        break;
                    case LongBreakTypeInterval:
                        createNotification("The long break is over, back to work!");
                        break;
                    default:
                        createNotification(
                            "The interval is finished, please report to Captain Rowl!"
                        );
                        break;
                }
            }
        }
    }

    renderAudio() {
        return (
            <audio id="myAudio">
                <source src={finishIntervalSound} type="audio/mpeg" />
                Your browser does not support the audio element.
            </audio>
        );
    }

    handleCloseNotificationAsking = () => {
        this.setState({
            isNotificationAskingOpen: false,
        });
    };

    onNotificationAskingYes = () => {
        this.handleCloseNotificationAsking();
        if (Notification.permission !== "granted") {
            requestNotificationPermission();
        }
    };

    renderNotificationPermissionAsking = () => {
        return (
            <Dialog
                open={this.state.isNotificationAskingOpen}
                // onClose={this.handleCloseNotificationAsking}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">Permission for notification</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        Please allow Rowl to use notification.
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button onClick={this.handleCloseNotificationAsking} color="primary">
                        No
                    </Button>
                    <Button onClick={this.onNotificationAskingYes} color="primary" autoFocus>
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
        );
    };

    renderReportFormModal = () => {
        return (
            <ReportFormModal
                open={this.state.isReportModalOpen}
                setOpen={this.setIsReportModalOpen}
            />
        );
    };

    renderMessageSnackbar = () => {
        return (
            <Snackbar
                anchorOrigin={{
                    vertical: "top",
                    horizontal: "center",
                }}
                // open={this.state.isMessageSnackbarOpen}
                open={this.props.isMessageSnackbarShow}
                autoHideDuration={6000}
                // onClose={() => this.props.closeMessage()}
                message={this.props.snackbarMessage}
                action={
                    <React.Fragment>
                        {/* <Button color="primary"
                            variant="contained"
                            size="small"
                            onClick={() => {
                                this.props.setRedirect(AP.Settings.Path);
                            }}
                        >
                            Settings
                        </Button> */}
                        <IconButton
                            size="small"
                            aria-label="close"
                            color="inherit"
                            onClick={() => this.props.closeMessage()}
                        >
                            <CloseIcon fontSize="small" />
                        </IconButton>
                    </React.Fragment>
                }
            />
        );
    };

    renderAppDrawer = () => {
        return <AppDrawer />;
    };

    render() {
        const placeholdingProps = {};
        return (
            <>
                {this.props.redirect !== "" && <Redirect to={this.props.redirect} push />}
                {this.renderAppDrawer()}
                {this.renderAppTop()}
                {this.renderAudio()}
                {this.renderNotificationPermissionAsking()}
                {this.renderReportFormModal()}
                {this.renderMessageSnackbar()}
                <Switch>
                    {APList.map((page) => {
                        if (page.Title === AP.Simple.Title) {
                            return (
                                <Route key={page.Path} exact path={page.Path}>
                                    <SimplePage setIsReportModalOpen={this.setIsReportModalOpen} />
                                    <Helmet>
                                        <title>{page.Title}</title>
                                    </Helmet>
                                </Route>
                            );
                        } else {
                            return (
                                <Route
                                    key={page.Path}
                                    exact
                                    path={page.Path}
                                    render={() => (
                                        <>
                                            <Helmet>
                                                <title>{page.Title}</title>
                                            </Helmet>
                                            {page.Render(placeholdingProps)}
                                        </>
                                    )}
                                ></Route>
                            );
                        }
                    })}
                    <Route>
                        <div>Nothing here brother</div>
                    </Route>
                </Switch>
                {this.renderBottomNavigation()}
            </>
        );
    }
}
const mapDispatchToProps = { setRedirect, popMessage, closeMessage };
const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        redirect: state.appReducer.redirect,
        triggerState: state.appReducer.triggerState,
        triggerType: state.appReducer.triggerType,
        isNotificationOn: state.settingReducer.isNotificationOn,
        isSoundOn: state.settingReducer.isSoundOn,

        isMessageSnackbarShow: state.snackbarReducer.show,
        snackbarMessage: state.snackbarReducer.message,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(AppRouter));
