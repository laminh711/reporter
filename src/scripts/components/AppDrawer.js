import { SwipeableDrawer } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import CloseIcon from "@material-ui/icons/Close";
import HelpIcon from "@material-ui/icons/Help";
import SentimentSatisfiedIcon from "@material-ui/icons/SentimentSatisfied";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { AP } from "../configs/AppPages";
import { SET_MULTIPURPOSE, TOGGLE_DRAWER } from "../redux/actionTypes/appActionTypes";


export default function AppDrawer() {
    const isDrawerOpen = useSelector((state) => state.appReducer.isDrawerOpen);
    const dispatch = useDispatch();

    const onCloseDrawer = () => {
        dispatch({
            type: TOGGLE_DRAWER,
            payload: false,
        });
    };

    const onOpenDrawer = () => {
        dispatch({
            type: TOGGLE_DRAWER,
            payload: true,
        });
    };

    const onClickFAQ = () => {
        dispatch({
            type: SET_MULTIPURPOSE,
            payload: {
                isDrawerOpen: false,
                redirect: AP.FAQ.Path,
            },
        });
    };

    const onClickAbout = () => {
        dispatch({
            type: SET_MULTIPURPOSE,
            payload: {
                isDrawerOpen: false,
                redirect: AP.About.Path,
            },
        });
    };

    const list = () => (
        <div
            role="presentation"
            // onClick={toggleDrawer(anchor, false)}
            // onKeyDown={toggleDrawer(anchor, false)}
        >
            <Button fullWidth endIcon={<CloseIcon />} onClick={onCloseDrawer}>
                Close
            </Button>
            <Divider />
            <List>
                <ListItem button onClick={onClickFAQ}>
                    <ListItemIcon>
                        <HelpIcon />
                    </ListItemIcon>
                    <ListItemText primary={"FAQ"} />
                </ListItem>
                <ListItem button onClick={onClickAbout}>
                    <ListItemIcon>
                        <SentimentSatisfiedIcon />
                    </ListItemIcon>
                    <ListItemText primary={"About"} />
                </ListItem>
            </List>
        </div>
    );

    return (
        <SwipeableDrawer
            anchor={"left"}
            open={isDrawerOpen}
            onOpen={onOpenDrawer}
            onClose={onCloseDrawer}
        >
            {list()}
        </SwipeableDrawer>
    );
}
