import React, { PureComponent } from "react";
import PropTypes from "prop-types";
import { IconButton, withStyles } from "@material-ui/core";
import { IsEmpty } from "../utils/StringUtil";
import { IsHollow } from "../utils/TypeUtil";

const style = {};

class AppIconButton extends PureComponent {
    static propTypes = {
        colorClass: PropTypes.string,
        edge: PropTypes.string,
        icon: PropTypes.element
    };

    constructor(props) {
        super(props);
        this.className = this.className.bind(this);
    }

    className() {
        const baseName = "app-icon-button";
        const add = !IsEmpty(this.props.colorClass) ? baseName + "--" + this.props.colorClass : "";
        return baseName + " " + add;
    }

    render() {
        return (
            <IconButton
                className={this.className()}
                edge={IsHollow(this.props.edge) ? false : this.props.edge}
            >
                {this.props.icon}
            </IconButton>
        );
    }
}

export default withStyles(style)(AppIconButton);
