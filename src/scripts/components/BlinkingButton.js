import React, { Component } from "react";
import PropTypes from "prop-types";
import { Button } from "@material-ui/core";

class BlinkingButton extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { variant, color, children } = this.props;
        return (
            <Button className="blinking-button" variant={variant} color={color}>
                {children}
            </Button>
        );
    }
}

export default BlinkingButton;
