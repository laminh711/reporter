import React, { PureComponent } from "react";
import PropTypes from "prop-types";

class BoomingProgress extends PureComponent {
    static propTypes = {
        text: PropTypes.string
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const { text } = this.props;
        return (
            <div className="booming-progress">
                <div className="booming-progress__boomer" />
                <div className="booming-progress__text">{text}</div>
            </div>
        );
    }
}

export default BoomingProgress;
