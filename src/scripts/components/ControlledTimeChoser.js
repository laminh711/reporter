import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import ArrowDropDownIcon from "@material-ui/icons/ArrowDropDown";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import Popper from "@material-ui/core/Popper";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";

import BlinkingButton from "../components/BlinkingButton";

/*
selectedOptionKey: 1
options: [
    {
        key: 1,
        value: "hahaha"
    }
]
*/

export default function ControlledTimeChoser(props) {
    const options = props.options;
    const selectedOptionKey = props.selectedOptionKey;
    const setSelectedOptionKey = props.setSelectedOptionKey;
    const isClockRunning = props.isClockRunning;
    const isClockNotSet = props.isClockNotSet;
    const printRemainTime = props.printRemainTime;

    const [open, setOpen] = React.useState(false);
    const anchorRef = React.useRef(null);

    const handleClick = () => {
        console.info(`You clicked ${options[selectedOptionKey]}`);
    };

    const handleMenuItemClick = (event, key) => {
        setSelectedOptionKey(key);
        setOpen(false);
    };

    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };

    const handleClose = (event) => {
        if (anchorRef.current && anchorRef.current.contains(event.target)) {
            return;
        }

        setOpen(false);
    };

    return (
        <Grid container direction="column" alignItems="center">
            <Grid item xs={12}>
                {isClockRunning() ? (
                    <BlinkingButton variant="contained" color="primary" content>
                        {printRemainTime()}
                    </BlinkingButton>
                ) : (
                    <ButtonGroup
                        variant="contained"
                        color="primary"
                        ref={anchorRef}
                        aria-label="split button"
                    >
                        <Button onClick={handleClick}>{options[selectedOptionKey].display}</Button>
                        <Button
                            color="primary"
                            size="small"
                            aria-controls={open ? "split-button-menu" : undefined}
                            aria-expanded={open ? "true" : undefined}
                            aria-label="select options"
                            aria-haspopup="menu"
                            onClick={handleToggle}
                        >
                            <ArrowDropDownIcon />
                        </Button>
                    </ButtonGroup>
                )}
                <Popper
                    open={open}
                    anchorEl={anchorRef.current}
                    role={undefined}
                    transition
                    disablePortal
                    style={{ zIndex: 100 }}
                >
                    {({ TransitionProps, placement }) => (
                        <Grow
                            {...TransitionProps}
                            style={{
                                transformOrigin:
                                    placement === "bottom" ? "center top" : "center bottom",
                            }}
                        >
                            <Paper>
                                <ClickAwayListener onClickAway={handleClose}>
                                    <MenuList id="split-button-menu">
                                        {options.map((option, index) => (
                                            <MenuItem
                                                key={option.key}
                                                selected={option.key === selectedOptionKey}
                                                onClick={(event) =>
                                                    handleMenuItemClick(event, option.key)
                                                }
                                            >
                                                {option.display}
                                            </MenuItem>
                                        ))}
                                    </MenuList>
                                </ClickAwayListener>
                            </Paper>
                        </Grow>
                    )}
                </Popper>
            </Grid>
        </Grid>
    );
}
