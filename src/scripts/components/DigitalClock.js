import React from "react";

export default class DigitalClock extends React.Component {
    printClock(clockNumbers) {
        return `${clockNumbers.hour < 10 ? "0" : ""}${clockNumbers.hour}:${
            clockNumbers.minute < 10 ? "0" : ""
        }${clockNumbers.minute}:${clockNumbers.second < 10 ? "0" : ""}${
            clockNumbers.second
        }`;
    }

    render() {
        let clockNumbersNumbers = this.props;
        return (
            <div>
                {clockNumbersNumbers !== null &&
                    this.printClock(clockNumbersNumbers)}
            </div>
        );
    }
}
