import { Typography } from "@material-ui/core";
import AddBox from "@material-ui/icons/AddBox";
import ArrowUpward from "@material-ui/icons/ArrowUpward";
import Check from "@material-ui/icons/Check";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import ChevronRight from "@material-ui/icons/ChevronRight";
import Clear from "@material-ui/icons/Clear";
import DeleteOutline from "@material-ui/icons/DeleteOutline";
import Edit from "@material-ui/icons/Edit";
import FilterList from "@material-ui/icons/FilterList";
import FirstPage from "@material-ui/icons/FirstPage";
import LastPage from "@material-ui/icons/LastPage";
import Print from "@material-ui/icons/Print";
import Remove from "@material-ui/icons/Remove";
import SaveAlt from "@material-ui/icons/SaveAlt";
import Search from "@material-ui/icons/Search";
import ViewColumn from "@material-ui/icons/ViewColumn";
import { format } from "date-fns";
import MaterialTable from "material-table";
import React, { forwardRef, useEffect, useRef, useState } from "react";
import { idb__getReport, idb__updateReport } from "../indexdb/idb";




function EditableLog(props) {
    let reportList = useRef([]);

    const tableColumns = [
        { title: "#", field: "id", editable: "never" },
        { title: "Start", field: "trigger_start", editable: "never" },
        { title: "End", field: "trigger_stop", editable: "never" },
        {
            title: "Work",
            field: "interval_description",
            type: "string",
        },
        {
            title: "Productive",
            field: "is_productive",
            type: "boolean",
        },
    ];

    const [tableData, setTableData] = useState([]);
    const [fetchReportList, setFetchReportList] = useState(false);

    useEffect(() => {
        if (!fetchReportList) {
            idb__getReport().then((list) => {
                setTableData(
                    list.map((report) => {
                        return {
                            id: report.id,
                            trigger_start: format(report.trigger_start, "HH:mm:ss"),
                            trigger_stop: format(report.trigger_stop, "HH:mm:ss"),
                            interval_description: report.interval_description,
                            is_productive: report.is_productive === 1 ? true : false,
                        };
                    })
                );
            }, []);
            setFetchReportList(true);
        }
    }, [tableData, fetchReportList]);

    const materialTableIcons = {
        Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} color="primary" />),
        Check: forwardRef((props, ref) => <Check {...props} ref={ref} color="primary" />),
        Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
        DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
        Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
        Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
        FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
        LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
        NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
        PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
        Print: forwardRef((props, ref) => <Print {...props} ref={ref} />),
        ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
        Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
        SortArrow: forwardRef((props, ref) => <ArrowUpward {...props} ref={ref} />),
        ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
        ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />),
    };

    return (
        <div className="editable-log">
            {props.withHeading && props.withHeading !== "" && (
                <div className="editable-log__heading">
                    <Typography variant="h6">{props.withHeading}</Typography>
                </div>
            )}
            <div className="editable-log__table">
                <MaterialTable
                    icons={materialTableIcons}
                    title="Logs"
                    columns={tableColumns}
                    data={tableData}
                    options={{ actionsColumnIndex: -1 }}
                    editable={{
                        onRowUpdate: (newData, oldData) =>
                            new Promise((resolve, reject) => {
                                setTimeout(() => {
                                    idb__updateReport(newData).then((v) => {
                                        setFetchReportList(false);
                                    });
                                    resolve();
                                }, 1000);
                            }),
                    }}
                />
            </div>
        </div>
    );
}

export default EditableLog;
