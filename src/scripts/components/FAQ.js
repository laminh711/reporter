import { Card, CardContent, Container, Divider, Grid, Link, Paper, Typography } from "@material-ui/core";
import BackupIcon from "@material-ui/icons/Backup";
import BarChartIcon from "@material-ui/icons/BarChart";
import MobileFriendlyIcon from "@material-ui/icons/MobileFriendly";
import OfflinePinIcon from "@material-ui/icons/OfflinePin";
import SettingsApplicationIcon from "@material-ui/icons/SettingsApplications";
import React from "react";

export default function FAQ() {
    return (
        <Container container={"true"} className="faq">
            <Grid>
                <Grid item xs={12} className="faq__content">
                    <Typography variant="h6">Q: What is Rowl?</Typography>
                    <Typography variant="body2">
                        <strong>Rowl</strong> is a pomodoro timer application, insprised by &nbsp;
                        <Link href={"https://tomato-timer.com/"} target="_blank">
                            tomato-timer
                        </Link>
                        .&nbsp;
                    </Typography>
                    <Divider />
                    <Typography variant="h6">Q: What is pomodoro technique?</Typography>
                    <Typography variant="body2">
                        <Link
                            href={"https://en.wikipedia.org/wiki/Pomodoro_Technique"}
                            target="_blank"
                        >
                            The Pomodoro Technique
                        </Link>
                        &nbsp; is a time management method developed by Francesco Cirillo in the
                        late 1980s. A pomodoro is the interval of working time spent. Basically,
                        there are 6 steps described by wikipedia:
                    </Typography>
                    <Card className={"faq__quote"}>
                        <CardContent className="faq__quotetext">
                            <ol>
                                <li>Decide on the task to be done</li>
                                <li>Set the pomodoro timer (usually 25 minutes)</li>
                                <li>Work on the task</li>
                                <li>
                                    End work when the timer rings and put a checkmark on a piece of
                                    paper.
                                </li>
                                <li>
                                    If you have fewer than four checkmarks, take a short break (3–5
                                    minutes), then go to step 2.
                                </li>
                                <li>
                                    After four pomodoros, take a longer break (15–30 minutes), reset
                                    your checkmark count to zero, then go to step 1.
                                </li>
                            </ol>
                        </CardContent>
                    </Card>
                    <Divider />
                    <Typography variant="h6">Q: What does Rowl offers?</Typography>
                    <Grid container item xs={12} className="faq">
                        <Grid xs={6} md={4} lg={3}>
                            <FeatureShowcase
                                renderIcon={() => <MobileFriendlyIcon color="primary" />}
                                text={"Mobile friendly UI"}
                            />
                        </Grid>
                        <Grid xs={6} md={4} lg={3}>
                            <FeatureShowcase
                                renderIcon={() => <OfflinePinIcon color="primary" />}
                                text={"Works offline"}
                            />
                        </Grid>
                        <Grid xs={6} md={4} lg={3}>
                            <FeatureShowcase
                                renderIcon={() => <SettingsApplicationIcon color="primary" />}
                                text={"Customizable"}
                            />
                        </Grid>
                        <Grid xs={6} md={4} lg={3}>
                            <FeatureShowcase
                                renderIcon={() => <BarChartIcon color="primary" />}
                                text={"Statistic (coming soon)"}
                            />
                        </Grid>
                        <Grid xs={6} md={4} lg={3}>
                            <FeatureShowcase
                                renderIcon={() => <BackupIcon color="primary" />}
                                text={"Cloud backup logs (coming soon)"}
                            />
                        </Grid>
                    </Grid>
                    <Divider />
                    <Typography variant="h6">Q: Why do I need backup?</Typography>
                    <Typography variant="body2">
                        <strong>Rowl</strong> use indexedDB to store logs. IndexedDB lives on browser so if you clear
                        browser's data, the logs will be gone.
                    </Typography>
                </Grid>
            </Grid>
        </Container>
    );
}

export function FeatureShowcase(props) {
    const { renderIcon, text } = props;
    return (
        <Paper className="feature-showcase">
            <Grid container>
                <Grid item xs={12} className="feature-showcase__icon">
                    {renderIcon()}
                </Grid>
                <Grid item xs={12} className="feature-showcase__text">
                    <Typography variant="subtitle2" align="center">
                        {text}
                    </Typography>
                </Grid>
            </Grid>
        </Paper>
    );
}
