import { CircularProgress, Typography } from "@material-ui/core";
import { differenceInSeconds } from "date-fns";
import React from "react";
import { useSelector } from "react-redux";
import { WorkTypeInterval } from "../redux/reducers/appReducer";

export default function IntervalProgress(props) {
    let value = 1;
    if (props.triggerStop != null) {
        const dif = differenceInSeconds(props.triggerStop, new Date());
        value = dif / props.totalTime;
        if (value < 0) value = 0;
    }

    const triggerType = useSelector((state) => state.appReducer.triggerType);
    const componentColor = triggerType === WorkTypeInterval ? "primary" : "secondary";

    return (
        <div className="interval-progress">
            <div className="interval-progress__inner">
                <Typography variant="body1">{props.printRemainTime()}</Typography>
            </div>
            <CircularProgress
                value={value * 100}
                className="interval-progress__circle"
                color={componentColor}
                variant="static"
                thickness={2.5}
            />
        </div>
    );
}
