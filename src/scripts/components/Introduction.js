import { Button, Card, CardContent, Container, Grid, Link, Typography } from "@material-ui/core";
import ArrowForwardIcon from "@material-ui/icons/ArrowForward";
import SettingsIcon from "@material-ui/icons/Settings";
import React from "react";
import { useDispatch } from "react-redux";
import logo from "../../assets/images/rowl-logo.png";
import { AP } from "../configs/AppPages";
import { setRedirect } from "../redux/actions/appActions";

function Introduction() {
    const dispatch = useDispatch();

    const onClickSimplePomodoro = () => {
        dispatch(setRedirect(AP.Simple.Path));
    };

    const onClickAutomatedPomodoro = () => {
        dispatch(setRedirect(AP.About.Path));
    };

    const onClickCustomizePomodoro = () => {
        dispatch(setRedirect(AP.Statistic.Path));
    };

    return (
        <Container container={"true"} className="introduction">
            <Grid>
                <Grid item xs={12} className="introduction__logo">
                    <img src={logo} alt="rowl" />
                </Grid>
                <Grid item xs={12} className="introduction__faq">
                    <Typography variant="body1">
                        <strong>Rowl</strong> is a pomodoro timer application, insprised by &nbsp;
                        <Link href={"https://tomato-timer.com/"} target="_blank">
                            tomato-timer
                        </Link>
                        .&nbsp;
                        <Link
                            href={"https://en.wikipedia.org/wiki/Pomodoro_Technique"}
                            target="_blank"
                        >
                            The Pomodoro Technique
                        </Link>
                        &nbsp; is a time management method developed by Francesco Cirillo in the
                        late 1980s. A pomodoro is the interval of working time spent. Basically,
                        there are 6 steps described by wikipedia:
                    </Typography>
                    <Card className={"introduction__quote"}>
                        <CardContent className="introduction__quotetext">
                            <ol>
                                <li>Decide on the task to be done</li>
                                <li>Set the pomodoro timer (usually 25 minutes)</li>
                                <li>Work on the task</li>
                                <li>
                                    End work when the timer rings and put a checkmark on a piece of
                                    paper.
                                </li>
                                <li>
                                    If you have fewer than four checkmarks, take a short break (3–5
                                    minutes), then go to step 2.
                                </li>
                                <li>
                                    After four pomodoros, take a longer break (15–30 minutes), reset
                                    your checkmark count to zero, then go to step 1.
                                </li>
                            </ol>
                        </CardContent>
                    </Card>
                    <Grid item xs={12} className="introduction__features">
                        <Typography variant="h6">Rowl's features</Typography>
                        <ul>
                            <li>Works offline as an app.</li>
                            <li>Mobile friendly UI.</li>
                            <li>Desktop notification with sound.</li>
                            <li>Customizable pomodoro timing.</li>
                            <li>A history of your activity.</li>
                            <li>Editable detail logs and cloud backup log (coming soon).</li>
                            <li>Charts! (coming soon)</li>
                        </ul>
                    </Grid>
                    <Grid item xs={12} className="introduction__navigation">
                        <Typography variant="h6">Start now!</Typography>
                        <Button
                            startIcon={<ArrowForwardIcon />}
                            color="primary"
                            onClick={onClickSimplePomodoro}
                        >
                            Simple pomodoro
                        </Button>
                        <Button
                            startIcon={<ArrowForwardIcon />}
                            color="primary"
                            onClick={onClickAutomatedPomodoro}
                        >
                            Automated pomodoro
                        </Button>
                        <Button
                            startIcon={<SettingsIcon />}
                            color="primary"
                            onClick={onClickCustomizePomodoro}
                        >
                            Customize pomodoro
                        </Button>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    );
}

export default Introduction;
