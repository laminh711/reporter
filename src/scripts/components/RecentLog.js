import { Button, IconButton, Typography } from "@material-ui/core";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import { add, format, isSameDay, sub } from "date-fns";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { idb__getReportByDate } from "../indexdb/idb";
import { SET_LOGS } from "../redux/actionTypes/appActionTypes";
import RecentLogItem from "./RecentLogItem";

export default function RecentLog(props) {
    const logs = useSelector((state) => state.appReducer.logs);
    const [fetched, setFetched] = useState(false);
    const dispatch = useDispatch();
    const [dateFilter, setDateFilter] = useState(new Date());

    useEffect(() => {
        if (!fetched) {
            idb__getReportByDate(dateFilter).then((logs) => {
                dispatch({ type: SET_LOGS, payload: logs });
            });
            setFetched(true);
        }
    }, [fetched, dateFilter, dispatch]);

    const onClickPrevious = (e) => {
        setDateFilter(sub(dateFilter, { days: 1 }));
        setFetched(false);
    };

    const onClickNext = (e) => {
        setDateFilter(add(dateFilter, { days: 1 }));
        setFetched(false);
    };

    const onClickJump = (e) => {
        setDateFilter(new Date());
        setFetched(false);
    };

    return (
        <div className="recent-log">
            <Typography variant="h6">Recent logs</Typography>
            <div className="recent-log__date">
                <IconButton size="small" onClick={onClickPrevious}>
                    <ArrowLeftIcon color="primary" />
                </IconButton>
                <Typography variant="subtitle1">{format(dateFilter, "MMM dd, yyyy")}</Typography>
                <IconButton
                    size="small"
                    onClick={onClickNext}
                    disabled={isSameDay(new Date(), dateFilter)}
                >
                    <ArrowRightIcon
                        color={isSameDay(new Date(), dateFilter) ? "disabled" : "primary"}
                    />
                </IconButton>

                <Button
                    variant="contained"
                    size="small"
                    color="primary"
                    className="recent-log__jump-button"
                    onClick={onClickJump}
                >
                    Today
                </Button>
            </div>
            <div className="recent-log__logs">
                {logs.length > 0 ? (
                    logs.map((log, index) => {
                        return (
                            <RecentLogItem
                                key={index}
                                isLast={index === logs.length - 1}
                                log={log}
                            />
                        );
                    })
                ) : (
                    <Typography variant="body1">No logs on this day</Typography>
                )}
            </div>
        </div>
    );
}
