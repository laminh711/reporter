import { Divider, Typography } from "@material-ui/core";
import AssignmentIcon from "@material-ui/icons/Assignment";
import FreeBreakfastIcon from "@material-ui/icons/FreeBreakfast";
import LocalBarIcon from "@material-ui/icons/LocalBar";
import OfflineBoltIcon from "@material-ui/icons/OfflineBolt";
import RemoveIcon from "@material-ui/icons/Remove";
import WarningIcon from "@material-ui/icons/Warning";
import { format } from "date-fns";
import React from "react";
import { BreakTypeInterval, LongBreakTypeInterval, WorkTypeInterval } from "../redux/reducers/appReducer";

export default function RecentLogItem(props) {
    const log = props.log;
    const isLast = props.isLast;

    const renderIntervalTypeIcon = (log) => {
        if (log.trigger_type === BreakTypeInterval) {
            return <FreeBreakfastIcon color="secondary" />;
        }
        if (log.trigger_type === LongBreakTypeInterval) {
            return <LocalBarIcon color="secondary" />;
        }
        if (log.trigger_type === WorkTypeInterval) {
            return <AssignmentIcon color="primary" />;
        }
    };

    const renderIsProductive = (log) => {
        if (log.trigger_interruption) {
            return <WarningIcon />;
        }
        return log.is_productive ? <OfflineBoltIcon /> : <RemoveIcon />;
    };

    const productiveSectionClass = (log) => {
        if (log.trigger_interruption) {
            return "recent-log-item__productive--interrupted";
        }
        return log.is_productive
            ? "recent-log-item__productive--productive"
            : "recent-log-item__productive--not-productive";
    };

    const intervalTypeText = (type) => {
        switch (type) {
            case WorkTypeInterval:
                return "Work";
            case BreakTypeInterval:
                return "Break";
            case LongBreakTypeInterval:
                return "Long break";
            default:
                return "Interval";
        }
    };

    return (
        <>
            <div className="recent-log-item">
                <div className="recent-log-item__worktype">
                    {renderIntervalTypeIcon(log)}
                    <Typography
                        variant="subtitle2"
                        color={log.trigger_type === WorkTypeInterval ? "primary" : "secondary"}
                    >
                        {intervalTypeText(log.trigger_type)}
                    </Typography>
                </div>
                <div className="recent-log-item__timeline">
                    <Typography variant="subtitle2">
                        {format(log.trigger_start, "HH:mm") +
                            "-" +
                            format(log.trigger_stop, "HH:mm")}
                    </Typography>
                </div>
                <div className={"recent-log-item__productive " + productiveSectionClass(log)}>
                    {renderIsProductive(log)}
                    <Typography variant="subtitle2">
                        {log.trigger_interruption
                            ? "Interrupted"
                            : log.is_productive
                            ? "Productive"
                            : "Decent"}
                    </Typography>
                </div>
            </div>
            {!isLast && <Divider />}
        </>
    );
}
