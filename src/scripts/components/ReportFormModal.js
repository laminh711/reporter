import { Checkbox, FormControlLabel } from "@material-ui/core";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import TextField from "@material-ui/core/TextField";
import { differenceInSeconds } from "date-fns";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { idb__addReport, idb__getReportByDate } from "../indexdb/idb";
import { SET_LOGS, SET_TRIGGER_TIME } from "../redux/actionTypes/appActionTypes";
import { NoInterval } from "../redux/reducers/appReducer";

export default function ReportFormModal(props) {
    const { open, setOpen } = props;

    const handleClose = () => {
        setOpen(false);
    };

    const [productive, setProductive] = React.useState(false);
    const [interrupted, setInterrupted] = React.useState(false);
    const [intervalDescription, setIntervalDescription] = React.useState("");

    const { triggerStart, triggerStop, triggerType } = useSelector(
        state => state.appReducer,
        () => false
    );

    const dispatch = useDispatch();

    const resetForm = () => {
        setProductive(false);
        setIntervalDescription("");
    };

    const onClickSubmit = () => {
        const recordTime = triggerStop;
        idb__addReport(
            {
                trigger_start: triggerStart,
                trigger_stop: triggerStop,
                trigger_type: triggerType,
                interval_description: intervalDescription,
                is_productive: productive,
                trigger_duration: differenceInSeconds(triggerStop, triggerStart),
                trigger_interruption: interrupted,
                is_report_skipped: false
            },
            () => {
                idb__getReportByDate(recordTime).then(logs => {
                    dispatch({ type: SET_LOGS, payload: logs });
                });
                // success callback
            },
            () => {
                // error callback
            }
        );

        dispatch({
            type: SET_TRIGGER_TIME,
            payload: {
                triggerStart: null,
                triggerStop: null,
                triggerState: NoInterval
            }
        });

        resetForm();
        handleClose();
    };

    const onClickSkip = () => {
        const recordTime = triggerStop;
        idb__addReport(
            {
                trigger_start: triggerStart,
                trigger_stop: triggerStop,
                trigger_type: triggerType,
                interval_description: intervalDescription,
                is_productive: productive,
                trigger_duration: differenceInSeconds(triggerStop, triggerStart),
                trigger_interruption: false,
                is_report_skipped: true
            },
            () => {
                // success callback
                idb__getReportByDate(recordTime).then(logs => {
                    dispatch({ type: SET_LOGS, payload: logs });
                });
            },
            () => {
                // error callback
                console.log("add error callback");
            }
        );
        dispatch({
            type: SET_TRIGGER_TIME,
            payload: {
                triggerStart: null,
                triggerStop: null,
                triggerState: NoInterval
            }
        });
        resetForm();
        handleClose();
    };

    const onProductiveChange = e => {
        setProductive(e.target.checked);
    };

    const onInterruptedChange = e => {
        setInterrupted(e.target.checked);
    };

    const onIntervalDescriptionChange = e => {
        setIntervalDescription(e.target.value);
    };

    return (
        <Dialog
            className="report-form-modal"
            open={open}
            // onClose={handleClose}
            aria-labelledby="form-dialog-title"
        >
            <DialogTitle id="form-dialog-title">Report your work</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    You've just finished your work interval, report what you've done and how
                    productive you were to track your performace. It's ok to skip this, you can edit
                    this later in the Log section.
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    label="Give a short description"
                    type="text"
                    fullWidth
                    value={intervalDescription}
                    onChange={onIntervalDescriptionChange}
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={productive}
                            onChange={onProductiveChange}
                            name="checkedProductive"
                        />
                    }
                    label="Was it productive?"
                />
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={interrupted}
                            onChange={onInterruptedChange}
                            name="checkedInterrupted"
                        />
                    }
                    label="Was it interrupted?"
                />
            </DialogContent>
            <DialogActions>
                <Button onClick={onClickSubmit} color="primary">
                    Submit
                </Button>
                <Button onClick={onClickSkip} color="primary">
                    Skip
                </Button>
            </DialogActions>
        </Dialog>
    );
}
