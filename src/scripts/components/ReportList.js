import { Avatar, List, ListItem, ListItemAvatar, ListItemText } from "@material-ui/core";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import { format } from "date-fns";
import PropTypes from "prop-types";
import React, { Component } from "react";
import { toLocalDate } from "../utils/time_util";

class ReportList extends Component {
    static propTypes = {
        reportList: PropTypes.arrayOf(PropTypes.object),
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <List className={"report-list"}>
                {this.props.reportList.map((report, index) => (
                    <ListItem key={index}>
                        <ListItemAvatar>
                            {report.isProductive ? (
                                <Avatar className="report-list__avatar--productive">
                                    <ArrowUpwardIcon />
                                </Avatar>
                            ) : (
                                <Avatar className="report-list__avatar--nonproductive">
                                    <ArrowDownwardIcon />
                                </Avatar>
                            )}
                        </ListItemAvatar>
                        <ListItemText
                            primary={report.intervalDescription}
                            secondary={
                                format(toLocalDate(report.triggerStart), "MMM dd") +
                                " | " +
                                format(toLocalDate(report.triggerStart), "HH:mm:ss") +
                                " - " +
                                format(toLocalDate(report.triggerStop), "HH:mm:ss")
                            }
                        />
                    </ListItem>
                ))}
            </List>
        );
    }
}

export default ReportList;
