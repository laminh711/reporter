import DateFnsUtils from "@date-io/date-fns";
import { Paper, Typography } from "@material-ui/core";
import Switch from "@material-ui/core/Switch";
import { MuiPickersUtilsProvider, TimePicker } from "@material-ui/pickers";
import { add, differenceInSeconds } from "date-fns";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { idb__updateAppSetting } from "../indexdb/idb";
import { requestNotificationPermission } from "../notifications";
import { SET_SETTINGS } from "../redux/actions/settingActions";
import { SET_TRIGGER_TIME } from "../redux/actionTypes/appActionTypes";
import { BreakTypeInterval, LongBreakTypeInterval, WorkTypeInterval } from "../redux/reducers/appReducer";
import { makeAlignedDate, referenceZero } from "../utils/TimeUtil";

export default function SettingSection() {
    const isSoundOn = useSelector((state) => state.settingReducer.isSoundOn);
    const isNotificationOn = useSelector((state) => state.settingReducer.isNotificationOn);
    const workDuration = useSelector((state) => state.settingReducer.workDuration);
    const breakDuration = useSelector((state) => state.settingReducer.breakDuration);
    const longBreakDuration = useSelector((state) => state.settingReducer.longBreakDuration);
    const triggerType = useSelector((state) => state.appReducer.triggerType);

    const dispatch = useDispatch();
    const handleNotificationToggle = (e) => {
        const ticked = e.target.checked;
        requestNotificationPermission().then((result) => {
            if (result === "granted") {
                idb__updateAppSetting({ is_notification_on: ticked }).then(
                    (updatedSettings) => {
                        dispatch({
                            type: SET_SETTINGS,
                            payload: { isNotificationOn: updatedSettings.is_notification_on },
                        });
                    }
                );
            } else {

            }
        });
    };
    const handleSoundToggle = (e) => {
        idb__updateAppSetting({ is_sound_on: e.target.checked }).then((updatedSettings) => {
            dispatch({
                type: SET_SETTINGS,
                payload: { isSoundOn: updatedSettings.is_sound_on },
            });
        });
    };

    const durationToPickerTime = (duration) => {
        let pickerTime = new Date();
        pickerTime.setHours(0);
        pickerTime.setMinutes(0);
        pickerTime.setSeconds(0);
        pickerTime = add(pickerTime, { seconds: duration });
        return pickerTime;
    };

    const pickerTimeToDuration = (date) => {
        const alignedToReferenceZero = makeAlignedDate(date);
        const ans = differenceInSeconds(alignedToReferenceZero, referenceZero);
        return ans;
    };

    const handleChangeWorkDuration = (newDate) => {
        const newWorkDuration = pickerTimeToDuration(newDate);
        idb__updateAppSetting({ work_duration: newWorkDuration }).then((updatedSettings) => {
            dispatch({
                type: SET_SETTINGS,
                payload: { workDuration: updatedSettings.work_duration },
            });
            if (triggerType === WorkTypeInterval) {
                dispatch({
                    type: SET_TRIGGER_TIME,
                    payload: { triggerDuration: newWorkDuration },
                });
            }
        });
    };
    const handleChangeBreakDuration = (newDate) => {
        const newBreakDuration = pickerTimeToDuration(newDate);
        idb__updateAppSetting({ break_duration: newBreakDuration }).then((updatedSettings) => {
            dispatch({
                type: SET_SETTINGS,
                payload: { breakDuration: updatedSettings.break_duration },
            });
            if (triggerType === BreakTypeInterval) {
                dispatch({
                    type: SET_TRIGGER_TIME,
                    payload: { triggerDuration: newBreakDuration },
                });
            }
        });
    };
    const handleChangeLongBreakDuration = (newDate) => {
        const newLongBreakDuration = pickerTimeToDuration(newDate);
        idb__updateAppSetting({ long_break_duration: newLongBreakDuration }).then(
            (updatedSettings) => {
                dispatch({
                    type: SET_SETTINGS,
                    payload: { longBreakDuration: updatedSettings.long_break_duration },
                });
                if (triggerType === LongBreakTypeInterval) {
                    dispatch({
                        type: SET_TRIGGER_TIME,
                        payload: { triggerDuration: newLongBreakDuration },
                    });
                }
            }
        );
    };

    return (
        <Paper className="setting-section" elevation={3}>
            <div className="setting-section__setting-item">
                <div className="setting-section__item-label">
                    <Typography variant="body1">Notification</Typography>
                </div>
                <div className="setting-section__item-input">
                    <Switch
                        color="primary"
                        edge="end"
                        onChange={handleNotificationToggle}
                        checked={isNotificationOn}
                    />
                </div>
            </div>
            <div className="setting-section__setting-item">
                <div className="setting-section__item-label">
                    <Typography variant="body1">Sound</Typography>
                </div>
                <div className="setting-section__item-input">
                    <Switch
                        color="primary"
                        edge="end"
                        onChange={handleSoundToggle}
                        checked={isSoundOn}
                    />
                </div>
            </div>
            <div className="setting-section__setting-item">
                <div className="setting-section__item-label">
                    <Typography variant="body1">Work duration (mm:ss)</Typography>
                </div>
                <div className="setting-section__item-input">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <TimePicker
                            ampm={false}
                            openTo="minutes"
                            views={["minutes", "seconds"]}
                            format="mm:ss"
                            value={durationToPickerTime(workDuration)}
                            minutesStep={5}
                            onChange={handleChangeWorkDuration}
                            className="setting-section__time-picker"
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </div>
            <div className="setting-section__setting-item">
                <div className="setting-section__item-label">
                    <Typography variant="body1">Break duration (mm:ss)</Typography>
                </div>
                <div className="setting-section__item-input">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <TimePicker
                            ampm={false}
                            openTo="minutes"
                            views={["minutes", "seconds"]}
                            format="mm:ss"
                            value={durationToPickerTime(breakDuration)}
                            minutesStep={5}
                            onChange={handleChangeBreakDuration}
                            className="setting-section__time-picker"
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </div>
            <div className="setting-section__setting-item">
                <div className="setting-section__item-label">
                    <Typography variant="body1">Long break duration (mm:ss)</Typography>
                </div>
                <div className="setting-section__item-input">
                    <MuiPickersUtilsProvider utils={DateFnsUtils}>
                        <TimePicker
                            ampm={false}
                            openTo="minutes"
                            views={["minutes", "seconds"]}
                            format="mm:ss"
                            value={durationToPickerTime(longBreakDuration)}
                            minutesStep={5}
                            onChange={handleChangeLongBreakDuration}
                            className="setting-section__time-picker"
                        />
                    </MuiPickersUtilsProvider>
                </div>
            </div>
        </Paper>
    );
}
