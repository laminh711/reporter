import Button from "@material-ui/core/Button";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import StopIcon from "@material-ui/icons/Stop";
import PropTypes from "prop-types";
import React, { Component } from "react";

class Timer extends Component {
    static propTypes = {
        startCallback: PropTypes.func,
        stopCallback: PropTypes.func,
        isClockSetAndFinish: PropTypes.func,
        isClockRunning: PropTypes.func,
    };

    constructor(props) {
        super(props);
        this.state = {};

        this.onClickStart = this.onClickStart.bind(this);
        this.onClickStop = this.onClickStop.bind(this);
    }

    componentDidMount() {}

    onClickStart() {
        this.props.startCallback();
    }

    onClickStop() {
        this.props.stopCallback();
    }

    render() {
        return (
            <div className="timer">
                <div className="timer__progress">{/* <BoomingProgress text="test" /> */}</div>
                <div className="timer__actions">
                    <Button
                        variant="contained"
                        color="primary"
                        endIcon={<PlayArrowIcon />}
                        onClick={this.onClickStart}
                        disabled={this.props.isClockRunning() || this.props.isClockSetAndFinish()}
                    >
                        Start
                    </Button>
                    <Button
                        variant="contained"
                        color="secondary"
                        endIcon={<StopIcon />}
                        onClick={this.onClickStop}
                        disabled={!this.props.isClockRunning() || this.props.isClockSetAndFinish()}
                    >
                        Stop
                    </Button>
                </div>
            </div>
        );
    }
}

export default Timer;
