import HomeIcon from "@material-ui/icons/Home";
import MoodIcon from "@material-ui/icons/Mood";
import OfflineBoltIcon from "@material-ui/icons/OfflineBolt";
import TimelineIcon from "@material-ui/icons/Timeline";
import AutorenewIcon from "@material-ui/icons/Autorenew";
import SettingsIcon from "@material-ui/icons/Settings";
import React from "react";
import AboutPage from "../pages/AboutPage";
import HomePage from "../pages/HomePage";
import OneTimePage from "../pages/OneTimePage";
import SetupPage from "../pages/SetupPage";
import StatisticPage from "../pages/StatisticPage";
import SimplePage from "../pages/SimplePage";
import AutomatedPage from "../pages/AutomatedPage";
import SettingsPage from "../pages/SettingsPage";
import FAQPage from "../pages/FAQPage";

const prefix = "Rowl - ";
export const AP = {
    FAQ: {
        Title: prefix + "FAQ",
        Alias: "FAQ",
        Path: "/faq",
        Component: FAQPage,
        Render: (props) => <FAQPage {...props} />,
        Icon: <HomeIcon />,
    },
    Home: {
        Title: prefix + "Home page",
        Alias: "Home",
        Path: "/home",
        Component: HomePage,
        Render: (props) => <HomePage {...props} />,
        Icon: <HomeIcon />,
    },
    OneTime: {
        Title: prefix + "One Time",
        Alias: "OneTime",
        Path: "/onetime",
        Component: OneTimePage,
        Render: (props) => <OneTimePage {...props} />,
        Icon: <OfflineBoltIcon />,
    },
    Statistic: {
        Title: prefix + "Statistic page",
        Alias: "Statistic",
        Path: "/statistic",
        Component: StatisticPage,
        Render: (props) => <StatisticPage {...props} />,
        Icon: <TimelineIcon />,
    },
    About: {
        Title: prefix + "About page",
        Alias: "OneTime",
        Path: "/about",
        Component: AboutPage,
        Render: (props) => <AboutPage {...props} />,
        Icon: <MoodIcon />,
    },
    Setup: {
        Title: prefix + "Setup page",
        Alias: "OneTime",
        Path: "/setup",
        Component: SetupPage,
        Render: (props) => <SetupPage {...props} />,
        // Icon:
    },
    Simple: {
        Title: prefix + "Timer",
        Alias: "Timer",
        Path: "/",
        Component: SimplePage,
        Render: (props) => <SimplePage {...props} />,
        Icon: <OfflineBoltIcon />,
    },
    Automated: {
        Title: prefix + "Automated page",
        Alias: "Automated",
        Path: "/automated",
        Component: AutomatedPage,
        Render: (props) => <AutomatedPage {...props} />,
        Icon: <AutorenewIcon />,
    },
    Settings: {
        Title: prefix + "Settings page",
        Alias: "Setting",
        Path: "/settings",
        Component: SettingsPage,
        Render: (props) => <SettingsPage {...props} />,
        Icon: <SettingsIcon />,
    },
};

export const APList = [
    AP.Home,
    AP.Automated,
    AP.OneTime,
    AP.Statistic,
    AP.About,
    AP.Setup,
    AP.Simple,
    AP.Settings,
    AP.FAQ
];

export const BNList = [AP.Simple, AP.Statistic, AP.Settings];
