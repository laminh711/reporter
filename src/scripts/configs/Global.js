export const DefaultWorkDuration = 5;
export const DefaultBreakDuration = 6;
export const DefaultLongBreakDuration = 7;