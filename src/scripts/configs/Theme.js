import { createMuiTheme } from "@material-ui/core/styles";

const blueeeeeTheme = createMuiTheme({
    palette: {
        primary: {
            // light: will be calculated from palette.primary.main,
            main: "#2892d7",
            // dark: will be calculated from palette.primary.main,
            // contrastText: will be calculated to contrast with palette.primary.main
        },
        secondary: {
            main: "#173753",
            // contrastText: '#ffcc00',
        },
    },

    overrides: {
        MuiCheckbox: {
            colorSecondary: {
                // color: "# custom color",
                "&$checked": {
                    color: "#2892d7",
                },
            },
        },
    },
});

export default blueeeeeTheme;
