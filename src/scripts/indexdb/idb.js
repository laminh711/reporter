import { isSameDay } from "date-fns";
import { openDB } from "idb";
import { DevBreakDuration, DevLongBreakDuration, DevWorkDuration } from "../configs/DevEnvGlobal";
import { envArr, isDevEnv } from "../utils/AppUtil";
const DB_NAME = "rowl";
const DB_VERSION = 2;
const DB_REPORT_STORE_NAME = "reports";
const DB_SETTING_STORE_NAME = "settings";

function reportSchema(db) {
    if (db.objectStoreNames.contains(DB_REPORT_STORE_NAME)) {
        return;
    }
    var store = db.createObjectStore(DB_REPORT_STORE_NAME, {
        keyPath: "id",
        autoIncrement: true,
    });
    store.createIndex("trigger_start", "trigger_start");
    store.createIndex("trigger_end", "trigger_end");
    store.createIndex("description", "description");
    store.createIndex("is_productive", "is_productive");
    store.createIndex("trigger_duration", "trigger_duration");
    store.createIndex("trigger_type", "trigger_type");
    store.createIndex("trigger_interruption", "trigger_interruption");
}

function settingSchema(db) {
    if (db.objectStoreNames.contains(DB_SETTING_STORE_NAME)) {
        return;
    }
    var store = db.createObjectStore(DB_SETTING_STORE_NAME, {
        keyPath: "id",
        autoIncrement: true,
    });
    store.createIndex("work_duration", "work_duration");
    store.createIndex("break_duration", "break_duration");
    store.createIndex("long_break_duration", "long_break_duration");
    store.createIndex("is_notification_on", "is_notification_on");
    store.createIndex("is_sound_on", "is_sound_on");
}

const defaultAppSetting = {
    work_duration: isDevEnv() ? DevWorkDuration : envArr("REACT_APP_WORK_DURATION"),
    break_duration: isDevEnv() ? DevBreakDuration : envArr("REACT_APP_BREAK_DURATION"),
    long_break_duration: isDevEnv() ? DevLongBreakDuration : envArr("REACT_APP_LONG_BREAK_DURATION"),
    is_notification_on: false,
    is_sound_on: false,
};

export async function idb__setDefaultAppSetting() {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });

    const transaction = db.transaction(DB_SETTING_STORE_NAME, "readwrite");
    const cursor = await transaction.store.openCursor();

    if (cursor === null) {
        transaction.store
            .add(defaultAppSetting)
            .then(() => {
                // success callback
            })
            .catch((e) => {
                console.log("there was an error while creating app seting");
                console.log(e);
            });
        return defaultAppSetting;
    } else {
        const returnValue = cursor.value;
        cursor.advance(1);
        return returnValue;
    }
}

export async function idb__updateAppSetting(newSetting) {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });

    const transaction = db.transaction(DB_SETTING_STORE_NAME, "readwrite");
    const cursor = await transaction.store.openCursor();
    if (cursor === null) {
        console.log("no setting was found :(");
        return defaultAppSetting;
    } else {
        let settings = cursor.value;
        settings = {
            ...settings,
            ...newSetting,
        };
        transaction.store.put(settings);
        return settings;
    }
}

export async function idb__getReportByDate(date) {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });

    let rs = [];
    let cursor = await db.transaction(DB_REPORT_STORE_NAME).store.openCursor();
    while (cursor) {
        const log = cursor.value;
        if (isSameDay(log.trigger_start, date)) {
            rs.push(cursor.value);
        }
        cursor = await cursor.continue();
    }
    return rs;
}

export async function idb__getReport() {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });

    let rs = [];
    let cursor = await db.transaction(DB_REPORT_STORE_NAME).store.openCursor();
    while (cursor) {
        rs.push(cursor.value);
        cursor = await cursor.continue();
    }
    return rs;
}

export async function idb__addReport(newReport, successCallback, errorCallback) {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });
    const store = db.transaction(DB_REPORT_STORE_NAME, "readwrite").store;
    store
        .add(newReport)
        .then((v) => {
            successCallback();
            console.log("success with");
            console.log(v);
        })
        .catch((e) => {
            errorCallback();
            console.log("error add");
            console.log(e);
        });
}

export async function idb__updateReport(newData) {
    const db = await openDB(DB_NAME, DB_VERSION, {
        upgrade(db, oldVersion, newVersion, transaction) {
            reportSchema(db);
            settingSchema(db);
        },
    });
    const value = await db.get(DB_REPORT_STORE_NAME, newData.id);
    value.is_productive = newData.is_productive ? 1 : 0;
    value.interval_description = newData.interval_description;
    // Set a value in a store:
    await db.put(DB_REPORT_STORE_NAME, value);
}
