import { getAppStore, reduxStore } from "../redux/reduxStore";
import {
    RunningInterval,
    NoInterval,
    FinishedInterval,
    WorkTypeInterval,
    FinishingInterval,
} from "../redux/reducers/appReducer";

import * as workerTimers from "worker-timers";
import { setTriggerTime, setLogs, setRedirect } from "../redux/actions/appActions";
import { add, differenceInSeconds } from "date-fns";
import { idb__addReport, idb__getReportByDate } from "../indexdb/idb";
import { AP } from "../configs/AppPages";

export function isClockRunning() {
    const appStore = getAppStore();
    return appStore.triggerState === RunningInterval;
}

export function isClockNotSet() {
    const appStore = getAppStore();
    return appStore.triggerState && appStore.triggerState === NoInterval;
}
export function isClockFinished() {
    const appStore = getAppStore();
    return appStore.triggerState && appStore.triggerState === FinishedInterval;
}
export function isClockFinishing() {
    const appStore = getAppStore();
    return appStore.triggerState && appStore.triggerState === FinishingInterval;
}

let interval = null;
let timeout = null;

export function startInterval(intervalCall, timeoutCall) {
    const appStore = getAppStore();

    // getAppStore().then((appStore) => {
    reduxStore.dispatch(
        setTriggerTime({
            triggerStart: new Date(),
            triggerStop: add(new Date(), { seconds: appStore.triggerDuration }),
            triggerState: RunningInterval,
        })
    );

    timeout = workerTimers.setTimeout(() => {
        onTimerEnds(timeoutCall);
    }, appStore.triggerDuration * 1000);
    // });
}

function onTimerEnds(timeoutCall) {
    const appStore = getAppStore();
    // getAppStore().then((appStore) => {
    if (appStore.triggerType === WorkTypeInterval) {
        reduxStore.dispatch(
            setTriggerTime({
                triggerState: FinishingInterval,
            })
        );
    } else {
        const recordTime = appStore.triggerStop;
        idb__addReport(
            {
                trigger_start: appStore.triggerStart,
                trigger_stop: appStore.triggerStop,
                trigger_type: appStore.triggerType,
                trigger_duration: differenceInSeconds(new Date(), appStore.triggerStart),
                trigger_interruption: false,
            },
            () => {
                idb__getReportByDate(recordTime).then((logs) => {
                    reduxStore.dispatch(setLogs(logs));
                });
                console.log("success callback");
            },
            () => {
                console.log("error callback");
            }
        );
        reduxStore.dispatch(
            setTriggerTime({
                triggerStart: null,
                triggerStop: null,
                triggerState: FinishingInterval,
            })
        );
    }
    timeoutCall();
    reduxStore.dispatch(setRedirect(AP.Simple.Path));
    // });
}

export function resetInterval() {
    const appStore = getAppStore();
    // getAppStore().then((appStore) => {
    const recordTime = appStore.triggerStop;
    if (appStore.triggerState === RunningInterval) {
        idb__addReport(
            {
                trigger_start: appStore.triggerStart,
                trigger_stop: new Date(),
                trigger_type: appStore.triggerType,
                trigger_duration: differenceInSeconds(new Date(), appStore.triggerStart),
                trigger_interruption: true,
                is_report_skipped: true,
            },
            () => {
                idb__getReportByDate(recordTime).then((logs) => {
                    reduxStore.dispatch(setLogs(logs));
                });
                console.log("success callback");
            },
            () => {
                console.log("error callback");
            }
        );
    }

    reduxStore.dispatch(
        setTriggerTime({
            triggerStart: null,
            triggerStop: null,
            triggerState: NoInterval,
        })
    );

    trashTimeout();
    // });
}

export function trashInterval() {
    if (interval != null) {
        workerTimers.clearInterval(interval);
        interval = null;
    }
}

export function trashTimeout() {
    if (timeout != null) {
        workerTimers.clearTimeout(timeout);
        timeout = null;
    }
}
