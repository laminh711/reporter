import { AppBar, Container, Toolbar } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import HelpIcon from "@material-ui/icons/Help";
import MenuIcon from "@material-ui/icons/Menu";
import React from "react";
import { useDispatch } from "react-redux";
import logo from "../../assets/images/rowl-logo.png";
import { AP } from "../configs/AppPages";
import { SET_REDIRECT, TOGGLE_DRAWER } from "../redux/actionTypes/appActionTypes";

export default function AppTop() {
    const dispatch = useDispatch();

    const onClickMenu = () => {
        dispatch({
            type: TOGGLE_DRAWER,
            payload: true,
        });
    };

    const onClickFAQ = () => {
        dispatch({
            type: SET_REDIRECT,
            payload: AP.FAQ.Path,
        });
    };

    return (
        <AppBar position="fixed" className="app-top">
            <Container maxWidth="lg" className="app-top__container">
                <Toolbar className="app-top__toolbar">
                    <IconButton edge="start" color="secondary" onClick={onClickMenu}>
                        <MenuIcon />
                    </IconButton>
                    <img src={logo} alt="Logo" className="app-top__logo" />
                    <IconButton edge="end" color="secondary" onClick={onClickFAQ}>
                        <HelpIcon />
                    </IconButton>
                </Toolbar>
            </Container>
        </AppBar>
    );
}
