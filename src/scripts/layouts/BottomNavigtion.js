import BottomNavigation from "@material-ui/core/BottomNavigation";
import BottomNavigationAction from "@material-ui/core/BottomNavigationAction";
import React, { PureComponent } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { BNList } from "../configs/AppPages";
import { setRedirect } from "../redux/actions/appActions";

class BottomNavigtion extends PureComponent {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {
            // value: AP.Home.Path
            value: this.props.location.pathname,
        };

        this.setValue = this.setValue.bind(this);
    }

    setValue(newValue) {
        this.setState({ value: newValue });
        this.props.setRedirect(newValue);
    }

    render() {
        const pathname = this.props.location.pathname;
        return (
            <BottomNavigation
                value={pathname}
                onChange={(event, newValue) => {
                    this.setValue(newValue);
                }}
                classes={{
                    root: "bottom-navigation",
                }}
                showLabels
            >
                {BNList.map((bn) => {
                    return (
                        <BottomNavigationAction
                            className="bottom-navigation__item"
                            key={bn.Path}
                            value={bn.Path}
                            label={bn.Alias}
                            icon={bn.Icon}
                        />
                    );
                })}
            </BottomNavigation>
        );
    }
}

const mapDispatchToProps = { setRedirect };

export default connect(null, mapDispatchToProps)(withRouter(BottomNavigtion));
