import React, { Component } from "react";
import PropTypes from "prop-types";

class CommonWrapper extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className="common-wrapper">{this.props.children}</div>;
    }
}

export default CommonWrapper;
