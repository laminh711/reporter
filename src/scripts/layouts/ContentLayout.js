import PropTypes from "prop-types";
import React, { Component } from "react";
import BottomNavigtion from "./BottomNavigtion";

class ContentLayout extends Component {
    static propTypes = {
    };

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return (
            <>
                {this.props.children}
            </>
        );
    }
}

export default ContentLayout;
