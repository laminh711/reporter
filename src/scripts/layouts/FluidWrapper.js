import React, { Component } from "react";
import PropTypes from "prop-types";

class FluidWrapper extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        return <div className="fluid-wrapper">{this.props.children}</div>;
    }
}

export default FluidWrapper;
