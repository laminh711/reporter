const ApiStatus = Object.freeze({
    STOP: Symbol("stop"),
    START: Symbol("start"),
    FINISH: Symbol("finish"),
    ERROR: Symbol("error")
});
export default ApiStatus;
