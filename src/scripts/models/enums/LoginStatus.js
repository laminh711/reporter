const LoginStatus = Object.freeze({
    NOTHING: Symbol("nothing"),
    SUCCESS: Symbol("success"),
    FAILED: Symbol("failed"),
    ERROR: Symbol("error")
});
export default LoginStatus;
