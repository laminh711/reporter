var webNotification;

export function createNotification(title) {
    webNotification = new Notification(title);
    webNotification.onclick = function () {
        window.focus();
    };
}

export function requestNotificationPermission() {
    return Notification.requestPermission();
}
