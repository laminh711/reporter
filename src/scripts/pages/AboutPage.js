import { Avatar, Divider, Link, Typography } from "@material-ui/core";
import React from "react";
import goodboysmall from "../../assets/images/goodboy_small.jpg";


export default class AboutPage extends React.Component {
    render() {
        return (
            <div className="about-page">
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <Link href="https://gitlab.com/laminh711" target="_blank">
                        <Avatar src={goodboysmall} style={{ height: "7rem", width: "7rem" }} />
                    </Link>
                </div>
                <Typography variant="body1" align="justify">
                    I am Minh Le, a software developer and the creator of this project. Feel free to
                    roast my code or shoot me a feedback through{" "}
                    <Link target="_blank" href="https://gitlab.com/laminh711/reporter">
                        gitlab
                    </Link>{" "}
                    or{" "}
                    <Link href="mailto:laminh711@gmail.com" target="_blank">
                        my email
                    </Link>
                    .
                </Typography>
                <Typography variant="body2" style={{ fontWeight: "bold" }} align="center">
                    Thank you for using my application!{" "}
                </Typography>
                <Typography variant="body2" style={{ fontWeight: "bold" }} align="center">
                    Have a nice day! :)
                </Typography>

                <Divider />
                <Typography variant="h5">Credits</Typography>
                <Typography variant="subtitle1">
                    Logo at{" "}
                    <Link target="_blank" href="https://seeklogo.com/vector-logo/371503/owl-guru">seeklogo</Link>
                </Typography>
                <Typography variant="subtitle1">
                    Image by{" "}
                    <Link target="_blank" href="https://pixabay.com/users/gyanbasnet-4192580/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4011849">
                        Gyan Bahadur Basnet
                    </Link>{" "}
                    from{" "}
                    <Link target="_blank" href="https://pixabay.com/?utm_source=link-attribution&amp;utm_medium=referral&amp;utm_campaign=image&amp;utm_content=4011849">
                        Pixabay
                    </Link>
                </Typography>
            </div>
        );
    }
}
