import React from "react";
import { Typography, Container } from "@material-ui/core";
import underConstruction from "../../assets/images/under-construction.png";

function AutomatedPage(props) {
    return (
        <div className="automated-page">
            <Container>
                <Typography variant="h4">Coming soon...</Typography>
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <img
                        style={{ width: "50%" }}
                        src={underConstruction}
                        alt="under construction"
                    />
                </div>
            </Container>
        </div>
    );
}

export default AutomatedPage;
