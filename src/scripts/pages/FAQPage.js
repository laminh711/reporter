import React from "react";
import FAQ from "../components/FAQ";

export default function FAQPage() {
    return (
        <div className="faq-page">
            <FAQ />
        </div>
    );
}
