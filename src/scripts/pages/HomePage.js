import React from "react";
import Introduction from "../components/Introduction";

function HomePage(props) {
    return (
        <div className="home-page">
            <Introduction />
        </div>
    );
}

export default HomePage;
