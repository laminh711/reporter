import {
    Button,
    Checkbox,
    Collapse,
    FormControlLabel,
    FormGroup,
    TextField,
    Typography,
} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import Slider from "@material-ui/core/Slider";
import { add, format } from "date-fns";
import { differenceInSeconds, subSeconds } from "date-fns/esm";
import React, { Component } from "react";
import { connect } from "react-redux";
import ReportList from "../components/ReportList";
import Timer from "../components/Timer";
import { setTriggerTime } from "../redux/actions/appActions";
import { FinishedInterval, NoInterval, RunningInterval } from "../redux/reducers/appReducer";
import { isZeroTime, makeHourTime, secondsToHourObject } from "../utils/time_util";
import { IsHollow } from "../utils/TypeUtil";
import * as workerTimers from "worker-timers";

const secondIntervalTime = 1000;
const minuteIntervalTime = 60000;

function valuetext(value) {
    return `${value} minutes`;
}

const marks = [
    {
        value: 25,
        label: "25mins",
    },
    {
        value: 60,
        label: "1hour",
    },
    {
        value: 120,
        label: "2 hours",
    },
    {
        value: 240,
        label: "4 hours",
    },
];

class OneTimePage extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {
            running: false,

            ticker: 0,

            remainer: makeHourTime(4, 0, 0),
            sliderMark: 240,

            time: new Date(),

            isProductive: false,
            intervalDescription: "",
        };

        this.onClickStart = this.onClickStart.bind(this);
        this.onClickStopButton = this.onClickStopButton.bind(this);

        this.onSliderChange = this.onSliderChange.bind(this);
        this.onStart = this.onStart.bind(this);
        this.onStop = this.onStop.bind(this);

        this.onChangeIsProductive = this.onChangeIsProductive.bind(this);
        this.onChangeIntervalDescription = this.onChangeIntervalDescription.bind(this);

        this.onSubmit = this.onSubmit.bind(this);

        this.isClockRunning = this.isClockRunning.bind(this);
        this.isClockSetAndFinish = this.isClockSetAndFinish.bind(this);

        this.successAddReportCallback = this.successAddReportCallback.bind(this);
    }

    componentDidMount() {
        this.checkAndContinuePreviousInterval();
    }

    componentDidUpdate(prevProps, prevState) {
        if (isZeroTime(this.state.remainer)) {
            if (!IsHollow(this.tickInterval) && !IsHollow(this.minuteInterval)) {
                this.props.setTriggerTime({
                    triggerState: FinishedInterval,
                });
                clearInterval(this.tickInterval);
                clearInterval(this.minuteInterval);
                this.tickInterval = null;
                this.minuteInterval = null;
            }
        }
    }

    componentWillUnmount() {
        clearInterval(this.tickInterval);
        clearInterval(this.minuteInterval);
    }

    successAddReportCallback() {
        alert("added");
    }

    onSubmit(e) {
        e.preventDefault();
       
        // call actions
        // promise fullfilled
        this.props.setTriggerTime({
            triggerState: NoInterval,
        });
        this.setState({
            remainer: makeHourTime(4, 0, 0),
            sliderMark: 240,
        });
    }

    onClickStart() {
        this.setState({
            running: true,
        });
    }

    onClickStopButton() {
        this.setState({
            running: false,
        });
    }

    onStop() {
        if (!IsHollow(this.tickInterval) && !IsHollow(this.minuteInterval)) {
            this.props.setTriggerTime({
                triggerStart: null,
                triggerStop: null,
                triggerState: NoInterval,
            });
            workerTimers.clearInterval(this.tickInterval);
            workerTimers.clearInterval(this.minuteInterval);

            this.setState({
                remainer: makeHourTime(4, 0, 0),
            });
        }
    }

    checkAndContinuePreviousInterval() {
        if (this.props.triggerState === RunningInterval) {
            const totalDiffInSeconds = differenceInSeconds(this.props.triggerStop, new Date());
            const difObj = secondsToHourObject(totalDiffInSeconds);
            const newRemainer = makeHourTime(difObj.hour, difObj.minute, difObj.second);
            this.setState({
                remainer: newRemainer,
                sliderMark: difObj.minute,
            });

            this.tickInterval = workerTimers.setInterval(() => {
                this.setState((prevState) => {
                    return {
                        remainer: subSeconds(prevState.remainer, 1),
                    };
                });
            }, secondIntervalTime);

            this.minuteInterval = workerTimers.setInterval(() => {
                this.setState((prevState) => {
                    return {
                        sliderMark: prevState.sliderMark - 1,
                    };
                });
            }, minuteIntervalTime);
        }
    }

    onStart() {
        this.tickInterval = setInterval(() => {
            this.setState((prevState) => {
                return {
                    remainer: subSeconds(prevState.remainer, 1),
                };
            });
        }, secondIntervalTime);

        this.minuteInterval = setInterval(() => {
            this.setState((prevState) => {
                return {
                    sliderMark: prevState.sliderMark - 1,
                };
            });
        }, minuteIntervalTime);

        const hours = this.state.remainer.getHours();
        const minutes = this.state.remainer.getMinutes();
        const seconds = this.state.remainer.getSeconds();
        this.props.setTriggerTime({
            triggerStart: new Date(),
            triggerStop: add(new Date(), {
                hours,
                minutes,
                seconds,
            }),
            triggerState: RunningInterval,
        });
    }

    isClockRunning() {
        return this.props.triggerState && this.props.triggerState === 2;
    }

    isClockSetAndFinish() {
        return this.props.triggerState === 1;
    }

    onSliderChange(e, v) {
        const hourvalue = Math.floor(v / 60);
        const minuteValue = v % 60;
        this.setState({
            sliderMark: v,
            remainer: makeHourTime(hourvalue, minuteValue, 0),
        });
    }

    onChangeIsProductive(e) {
        this.setState({
            isProductive: e.target.checked,
        });
    }

    onChangeIntervalDescription(e) {
        this.setState({
            intervalDescription: e.target.value,
        });
    }

    render() {
        return (
            <div className="one-time-page">
                <Grid container justify="center">
                    <Typography variant={"h6"} color={"textPrimary"}>
                        {this.isClockRunning()
                            ? "Running..."
                            : this.isClockSetAndFinish()
                            ? "Please enter the interval work detail"
                            : "Set the duration first and start working!"}
                    </Typography>
                </Grid>
                <Grid container justify="center">
                    <Typography variant={"h5"} color={"textPrimary"}>
                        {format(this.state.remainer, "HH:mm:ss")}
                    </Typography>
                </Grid>
                <Grid container justify="center">
                    <Grid item container xs={10} justify="center">
                        <Slider
                            onChange={this.onSliderChange}
                            defaultValue={240}
                            getAriaValueText={valuetext}
                            step={1}
                            min={1}
                            max={240}
                            value={this.state.sliderMark}
                            marks={marks}
                        />
                    </Grid>
                </Grid>
                <Grid container>
                    <Grid container item xs={12} justify={"center"}>
                        <Timer
                            startCallback={this.onStart}
                            stopCallback={this.onStop}
                            isClockSetAndFinish={this.isClockSetAndFinish}
                            isClockRunning={this.isClockRunning}
                        />
                    </Grid>
                </Grid>

                <Grid container justify="center">
                    <Collapse in={this.isClockSetAndFinish()}>
                        {/* <Collapse in={true}> */}
                        <form onSubmit={this.onSubmit}>
                            <FormGroup>
                                <TextField
                                    value={this.state.intervalDescription}
                                    onChange={this.onChangeIntervalDescription}
                                />
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.isProductive}
                                            onChange={this.onChangeIsProductive}
                                            name="checkedB"
                                            color="primary"
                                        />
                                    }
                                    label="Productive"
                                />
                                <Button variant="contained" color="primary" type="submit">
                                    Submit
                                </Button>
                            </FormGroup>
                        </form>
                    </Collapse>
                </Grid>
                <Grid container justify="center">
                    <ReportList reportList={this.props.reportList} />
                </Grid>
            </div>
        );
    }
}

const mapDispatchToProps = { setTriggerTime };
const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        triggerStart: state.appReducer.triggerStart,
        triggerStop: state.appReducer.triggerStop,
        triggerState: state.appReducer.triggerState,
        reportList: state.reportReducer.reportList,
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(OneTimePage);
