import { Container, Grid, Typography } from "@material-ui/core";
import React from "react";
import SettingSection from "../components/SettingSection";

function SettingsPage(props) {
    return (
        <div className="settings-page">
            <Container>
                <Grid container justify="center">
                    <Grid item xs={12} md={6}>
                        <Typography variant="h5" className="settings-page__title">
                            Settings
                        </Typography>
                        <SettingSection />
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
}

export default SettingsPage;
