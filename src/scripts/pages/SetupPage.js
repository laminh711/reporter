import Button from "@material-ui/core/Button";
import Step from "@material-ui/core/Step";
import StepLabel from "@material-ui/core/StepLabel";
import Stepper from "@material-ui/core/Stepper";
import React, { PureComponent } from "react";

class SetupPage extends PureComponent {
    static propTypes = {};

    constructor(props) {
        super(props);

        this.state = {
            activeStep: 0
        };

        this.renderStepContent = this.renderStepContent.bind(this);
        this.onClickNext = this.onClickNext.bind(this);
        this.onClickPreviuos = this.onClickPreviuos.bind(this);
    }

    onClickNext() {
        if (this.state.activeStep < 3) {
            this.setState(prevState => ({
                activeStep: prevState.activeStep + 1
            }));
        }
    }

    onClickPreviuos() {
        if (this.state.activeStep > 0) {
            this.setState(prevState => ({
                activeStep: prevState.activeStep - 1
            }));
        }
    }

    renderStepContent() {
        switch (this.state.activeStep) {
            case 0:
                return <div>first</div>;
            case 1:
                return <div>second</div>;
            case 2:
                return <div>third</div>;
            default:
                return <div>???</div>;
        }
    }

    render() {
        const steps = [
            "Choose your active days",
            "Choose your active hours",
            "Choose your interval"
        ];
        return (
            <div style={{ marginTop: "100px" }}>
                <Stepper activeStep={this.state.activeStep} alternativeLabel>
                    {steps.map(label => (
                        <Step key={label}>
                            <StepLabel>{label}</StepLabel>
                        </Step>
                    ))}
                </Stepper>

                {this.renderStepContent()}

                <Button color="primary" variant="outlined" onClick={this.onClickPreviuos}>
                    Back
                </Button>
                <Button onClick={this.onClickNext}>Next</Button>
            </div>
        );
    }
}

export default SetupPage;
