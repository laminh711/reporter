import { Button, Container, Paper, Tab, Tabs } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import AssignmentIcon from "@material-ui/icons/Assignment";
import FreeBreakfastIcon from "@material-ui/icons/FreeBreakfast";
import LocalBarIcon from "@material-ui/icons/LocalBar";
import { differenceInSeconds } from "date-fns/esm";
import React, { Component } from "react";
import { connect } from "react-redux";
import * as workerTimers from "worker-timers";
import IntervalProgress from "../components/IntervalProgress";
import RecentLog from "../components/RecentLog";
import { isClockRunning, resetInterval, startInterval } from "../interval/ivl";
import { setLogs, setTriggerTime } from "../redux/actions/appActions";
import {
    BreakTypeInterval,
    FinishedInterval,
    FinishingInterval,
    LongBreakTypeInterval,
    NoInterval,
    RunningInterval,
    WorkTypeInterval
} from "../redux/reducers/appReducer";
import { totalSecondsToHMS } from "../utils/TimeUtil";

// positive numbers only
function print2Digits(number) {
    if (number < 10) return "0" + number;
    else return number;
}

class SimplePage extends Component {
    static propTypes = {};

    constructor(props) {
        super(props);
        this.state = {
            selectedOptionKey: 0,
            intervalType: WorkTypeInterval
        };
        this.iv = null;
    }

    onChangeIntervalType = (e, newValue) => {
        const settings = this.props.settings;
        let duration = settings.workDuration;
        switch (newValue) {
            case BreakTypeInterval:
                duration = settings.breakDuration;
                break;
            case LongBreakTypeInterval:
                duration = settings.longBreakDuration;
                break;
            default:
                break;
        }
        this.props.setTriggerTime({
            triggerType: newValue,
            triggerDuration: duration
        });

        this.setState({
            intervalType: newValue
        });
    };

    setSelectedOptionKey = key => {
        this.setState({ selectedOptionKey: key });
    };

    componentDidMount() {
        if (isClockRunning() && differenceInSeconds(this.props.triggerStop, new Date()) > 0) {
            this.iv = workerTimers.setInterval(() => {
                this.forceUpdate();
            }, 1000);
        }

        this.setState({
            intervalType: this.props.triggerType
        });
    }

    componentWillUnmount() {
        if (this.iv != null) {
            workerTimers.clearInterval(this.iv);
            this.iv = null;
        }
    }

    onStart = () => {
        const callback = () => {};

        const timeoutCallback = () => {
            if (this.props.triggerType === WorkTypeInterval) {
                this.props.setIsReportModalOpen(true);
            }
            workerTimers.clearInterval(this.iv);
            this.iv = null;
        };
        startInterval(callback, timeoutCallback);
        this.iv = workerTimers.setInterval(() => {
            this.forceUpdate();
        }, 1000);
    };

    onReset = () => {
        if (this.iv != null) {
            workerTimers.clearInterval(this.iv);
            this.iv = null;
        }
        resetInterval();
    };

    printRemainTime = () => {
        const dateLeft = this.props.triggerStop;

        let totalSeconds = this.props.triggerDuration;
        if (dateLeft != null) {
            const dateRight = new Date();
            totalSeconds = differenceInSeconds(dateLeft, dateRight);
        }

        let { hours, minutes, seconds } = totalSecondsToHMS(totalSeconds);
        // issue
        if (seconds < 0) seconds = 0;

        return `${print2Digits(hours)}:${print2Digits(minutes)}:${print2Digits(seconds)}`;
    };

    printText = () => {
        switch (this.props.triggerState) {
            case NoInterval:
                return "Please choose the duration first";
            case RunningInterval:
                return this.printRemainTime();
            case FinishingInterval:
                return "Report to Capt. Rowl";
            case FinishedInterval:
                return "!";
            default:
                return "Ohaiyo";
        }
    };

    render() {
        return (
            <>
                <div className="simple-page">
                    <Container className="simple-page__action-blocks">
                        <Grid container justify="center">
                            <Paper>
                                <Tabs
                                    value={this.state.intervalType}
                                    indicatorColor="primary"
                                    textColor="primary"
                                    onChange={this.onChangeIntervalType}
                                    variant="fullWidth"
                                >
                                    <Tab
                                        label="Work"
                                        icon={<AssignmentIcon />}
                                        value={WorkTypeInterval}
                                        disabled={
                                            isClockRunning() &&
                                            this.props.triggerType !== WorkTypeInterval
                                        }
                                    />
                                    <Tab
                                        label="Short break"
                                        icon={<FreeBreakfastIcon />}
                                        value={BreakTypeInterval}
                                        disabled={
                                            isClockRunning() &&
                                            this.props.triggerType !== BreakTypeInterval
                                        }
                                    />
                                    <Tab
                                        label="Long break"
                                        icon={<LocalBarIcon />}
                                        value={LongBreakTypeInterval}
                                        disabled={
                                            isClockRunning() &&
                                            this.props.triggerType !== LongBreakTypeInterval
                                        }
                                    />
                                </Tabs>
                            </Paper>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item container xs={12} md={6} justify="center">
                                <IntervalProgress
                                    insideText={this.printText()}
                                    triggerStart={this.props.triggerStart}
                                    triggerStop={this.props.triggerStop}
                                    totalTime={this.props.triggerDuration}
                                    printRemainTime={this.printRemainTime}
                                />
                            </Grid>
                        </Grid>
                        <Grid container justify="center" spacing={2}>
                            <Grid item>
                                <Button
                                    disabled={isClockRunning()}
                                    variant="contained"
                                    color="primary"
                                    onClick={this.onStart}
                                >
                                    Start
                                </Button>
                            </Grid>

                            <Grid item>
                                <Button variant="outlined" color="primary" onClick={this.onReset}>
                                    Reset
                                </Button>
                            </Grid>
                        </Grid>
                        <Grid container justify="center">
                            <Grid item xs={12} md={6}>
                                <RecentLog />
                                {/* <EditableLog /> */}
                            </Grid>
                        </Grid>
                    </Container>
                </div>
            </>
        );
    }
}

const mapDispatchToProps = { setTriggerTime, setLogs };
const mapStateToProps = (state, ownProps) => {
    return {
        ...ownProps,
        triggerStart: state.appReducer.triggerStart,
        triggerStop: state.appReducer.triggerStop,
        triggerState: state.appReducer.triggerState,
        triggerDuration: state.appReducer.triggerDuration,
        triggerType: state.appReducer.triggerType,
        triggerInterruption: state.appReducer.triggerInterruption,
        reportList: state.reportReducer.reportList,
        settings: state.settingReducer
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(SimplePage);
