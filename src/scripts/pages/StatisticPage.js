import React, { useState } from "react";
import { Typography, Container, Collapse, Paper } from "@material-ui/core";
import underConstruction from "../../assets/images/under-construction.png";
import { HorizontalBar } from "react-chartjs-2";

const data = {
    labels: [
        "1:00",
        "2:00",
        "3:00",
        "4:00",
        "5:00",
        "6:00",
        "7:00",
        "8:00",
        "9:00",
        "10:00",
        "11:00",
        "12:00",
        "13:00",
        "14:00",
        "15:00",
    ],
    datasets: [
        {
            label: "",
            backgroundColor: function (context) {
                var index = context.dataIndex;
                var value = context.dataset.data[index];
                return value <= 25
                    ? "red" // draw negative values in red
                    : value <= 50
                    ? "yellow" // else, alternate values in blue and green
                    : "green";
            },
            // borderColor: "rgba(255,99,132,1)",
            // borderWidth: 1,
            // hoverBackgroundColor: "rgba(255,99,132,0.4)",
            // hoverBorderColor: "rgba(255,99,132,1)",
            barPercentage: 0.5,
            barThickness: 20,
            maxBarThickness: 20,
            minBarLength: 2,

            data: [0, 50, 50, 75, 100, 50, 75, 25, 25, 100, 75, 25, 0, 25, 0],
        },
    ],
};
function StatisticPage(props) {
    const [isPHourShow, setIsPHourShow] = useState(false);
    return (
        <div className="statistic-page">
            <Container>
                <Typography variant="h4">Coming soon...</Typography>
                <div style={{ display: "flex", justifyContent: "center" }}>
                    <img
                        style={{ width: "50%" }}
                        src={underConstruction}
                        alt="under construction"
                    />
                </div>
            </Container>
            <Container style={{ display: "none" }}>
                <Typography variant="h5" onClick={() => setIsPHourShow((prevShow) => !prevShow)}>
                    Productive rate on hours
                </Typography>
                <Collapse in={isPHourShow}>
                    <Paper elevation={4}>
                        <HorizontalBar
                            height={600}
                            data={data}
                            options={{
                                maintainAspectRatio: false,
                                scales: {
                                    xAxes: [
                                        {
                                            ticks: {
                                                callback: function (tick) {
                                                    return tick.toString() + "%";
                                                },
                                            },
                                        },
                                    ],
                                },
                            }}
                        />
                    </Paper>
                </Collapse>
            </Container>
        </div>
    );
}

export default StatisticPage;
