import React, { Component } from "react";
import PropTypes from "prop-types";

class PrefabBlock extends Component {
    static Header = ({ children, ...props }) => (
        <div className="header" {...props}>
            {children}
        </div>
    );
    static Body = ({ children, ...props }) => (
        <div className="body" {...props}>
            {children}
        </div>
    );
    static Footer = ({ children, ...props }) => (
        <div className="footer" {...props}>
            {children}
        </div>
    );
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const customClassName = this.props.customClassName + " block";
        return <div className={customClassName}>{this.props.children}</div>;
    }
}

PrefabBlock.propTypes = {
    customClassName: PropTypes.string
};

export default PrefabBlock;
