import React, { Component } from "react";
import PropTypes from "prop-types";

class PrefabPage extends Component {
    static Header = ({ children }) => <div className="header">{children}</div>;
    static Body = ({ children }) => <div className="body">{children}</div>;
    static Footer = ({ children }) => <div className="footer">{children}</div>;

    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const customClassName = this.props.customClassName + " page";

        return <div className={customClassName}>{this.props.children}</div>;
    }
}

PrefabPage.propTypes = {
    customClassName: PropTypes.string
};

export default PrefabPage;
