export const SET_REDIRECT = "SET_REDIRECT";
export const SET_TIMER = "SET_TIMER";
export const SET_TRIGGER_TIME = "SET_TRIGGER_TIME";
export const SET_REPORT_LIST = "SET_REPORT_LIST";

export const TOGGLE_DRAWER = "TOGGLE_DRAWER";

export const SET_INDEXEDDB = "SET_INDEXEDDB";
export const SET_LOGS = "SET_LOGS";
export const ADD_LOG = "ADD_LOG";

export const SET_MULTIPURPOSE = "SET_MULTIPURPOSE";