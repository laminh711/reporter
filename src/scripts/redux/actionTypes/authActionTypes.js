export const SET_DEFAULT_AUTH = "SET_DEFAULT_AUTH";
export const REQUEST_LOGIN = "REQUEST_LOGIN";
export const LOGIN = "LOGIN";
export const LOGIN_SUCCESS = "LOGIN_SUCCESS";
export const LOGIN_ERROR = "LOGIN_ERROR";
export const LOGIN_WRONG = "LOGIN_WRONG";

export const REQUEST_LOGOUT = "REQUEST_LOGOUT";
export const LOGOUT = "LOGOUT";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_ERROR = "LOGOUT_ERROR";
