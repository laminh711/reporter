import { SET_SETTINGS } from "../actions/settingActions";

export function setSettings(settings) {
    return {
        type: SET_SETTINGS,
        payload: settings,
    };
}
