import {
    SET_INDEXEDDB,
    SET_REDIRECT,
    SET_TIMER,
    SET_TRIGGER_TIME,
    SET_LOGS,
    ADD_LOG,
    TOGGLE_DRAWER,
    SET_MULTIPURPOSE,
} from "../actionTypes/appActionTypes";

export function setRedirect(payload) {
    return {
        type: SET_REDIRECT,
        payload,
    };
}

export function setTriggerTime(payload) {
    return {
        type: SET_TRIGGER_TIME,
        payload,
    };
}

export function setTimer(payload) {
    return {
        type: SET_TIMER,
        payload,
    };
}

export function setIndexedDb(payload) {
    return {
        type: SET_INDEXEDDB,
        payload,
    };
}

export function setLogs(payload) {
    return {
        type: SET_LOGS,
        payload,
    };
}

export function addLog(payload) {
    return {
        type: ADD_LOG,
        payload,
    };
}

export function toggleDrawer(payload) {
    return {
        type: TOGGLE_DRAWER,
        payload,
    };
}

export function setMultipurpose(payload) {
    return {
        type: SET_MULTIPURPOSE,
        payload,
    };
}