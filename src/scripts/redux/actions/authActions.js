import {
    REQUEST_LOGIN,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    LOGIN_WRONG,
    SET_DEFAULT_AUTH,
    REQUEST_LOGOUT,
    LOGOUT_ERROR,
    LOGOUT_SUCCESS
} from "../actionTypes/authActionTypes";
import { fetchWorktime } from "../actions/worktimeActions";
import { AuthAPI } from "../../utils/api";
import ApiStatus from "../../models/enums/ApiStatus";

/* Login Actions */
export function setDefaultAuth(payload) {
    return {
        type: SET_DEFAULT_AUTH,
        payload
    };
}

export function requestLogin() {
    return {
        type: REQUEST_LOGIN
    };
}

export function loginSuccess(payload) {
    return {
        type: LOGIN_SUCCESS,
        payload
    };
}

export function loginWrong() {
    return {
        type: LOGIN_WRONG
    };
}

export function loginError(error) {
    return {
        type: LOGIN_ERROR,
        error
    };
}

export function login(payload) {
    return dispatch => {
        dispatch(requestLogin());
        return AuthAPI.login(payload.username, payload.password).then(
            response => {
                if (response.message === "ok") {
                    const basicAuthToken = btoa(payload.username + ":" + payload.password);
                    localStorage.setItem("authToken", basicAuthToken);

                    dispatch(loginSuccess(btoa(payload.username + ":" + payload.password)));
                    dispatch(fetchWorktime());
                } else {
                    dispatch(loginWrong());
                }
            },
            error => {
                dispatch(loginError(error));
            }
        );
    };
}

/* Logout Actions */
export function requestLogout() {
    return {
        type: REQUEST_LOGOUT
    };
}

export function logoutSuccess(payload) {
    return {
        type: LOGOUT_SUCCESS,
        payload
    };
}

export function logoutError(error) {
    return {
        type: LOGOUT_ERROR,
        error
    };
}

export function logout(payload) {
    return dispatch => {
        dispatch(requestLogout());

        dispatch(
            setDefaultAuth({
                data: null,
                status: ApiStatus.STOP,
                error: null
            })
        );

        localStorage.clear();

        dispatch(
            logoutSuccess({
                data: null,
                status: ApiStatus.STOP,
                error: null
            })
        );
    };
}
