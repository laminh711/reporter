import { SET_REPORT_LIST, ADD_REPORT } from "../actionTypes/reportActionTypes";

export function setReportList(payload) {
    return {
        type: SET_REPORT_LIST,
        payload,
    };
}

export function addReport(payload) {
    return {
        type: ADD_REPORT,
        payload,
    };
}
