import { POP_MESSAGE, CLOSE_MESSAGE } from "../actionTypes/snackbarActionTypes";

export function popMessage(payload) {
    return {
        type: POP_MESSAGE,
        payload: payload,
    };
}

export function closeMessage() {
    return {
        type: CLOSE_MESSAGE,
    };
}
