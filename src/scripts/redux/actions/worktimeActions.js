import { WorktimeAPI } from "../../utils/api";
import { REQUEST_WORKTIME, RECEIVE_WORKTIME } from "../actionTypes/worktimeActionTypes";
import { RECEIVE_ERROR } from "../actionTypes/errorActionTypes";

export function requestWorktime() {
    return {
        type: REQUEST_WORKTIME
    };
}

export function receiveWorktime(payload) {
    return {
        type: RECEIVE_WORKTIME,
        payload
    };
}

export function receiveError(error) {
    return {
        type: RECEIVE_ERROR,
        error
    };
}

export function fetchWorktime() {
    return dispatch => {
        dispatch(requestWorktime());

        return WorktimeAPI.getWorkTime().then(
            response => {
                dispatch(receiveWorktime(response));
            },
            error => {
                dispatch(receiveError(error));
            }
        );
    };
}
