import {
    SET_INDEXEDDB,
    SET_REDIRECT,
    SET_TRIGGER_TIME,
    SET_LOGS,
    ADD_LOG,
    TOGGLE_DRAWER,
    SET_MULTIPURPOSE,
} from "../actionTypes/appActionTypes";

export const NoInterval = 0;
export const FinishedInterval = 1;
export const RunningInterval = 2;
export const FinishingInterval = 3;

export const UnsetTypeInterval = -1;
export const WorkTypeInterval = 0;
export const BreakTypeInterval = 1;
export const LongBreakTypeInterval = 2;

const defaultState = {
    indexedDB: null,
    redirect: "",
    timer: 0,
    triggerStart: null,
    triggerStop: null,
    triggerState: NoInterval, // 0: no interval; 1: interval finish; 2: interval running
    triggerType: WorkTypeInterval,
    triggerDuration: 60 * 25,
    triggerInterruption: false,

    isDrawerOpen: false,

    logs: [],
};

export function appReducer(state = defaultState, action) {
    switch (action.type) {
        case SET_REDIRECT: {
            return {
                ...state,
                redirect: action.payload,
            };
        }
        case SET_TRIGGER_TIME: {
            return {
                ...state,
                ...action.payload,
            };
        }
        case SET_LOGS: {
            return {
                ...state,
                logs: action.payload,
            };
        }
        case ADD_LOG: {
            return {
                ...state,
                logs: state.logs.push(action.payload),
            };
        }
        case TOGGLE_DRAWER: {
            return {
                ...state,
                isDrawerOpen: action.payload,
            };
        }
        case SET_INDEXEDDB: {
            return {
                ...state,
                indexedDB: action.payload,
            };
        }
        case SET_MULTIPURPOSE: {
            return {
                ...state,
                ...action.payload,
            };
        }
        default:
            return state;
    }
}
