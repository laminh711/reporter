import ApiStatus from "../../models/enums/ApiStatus";
import {
    REQUEST_LOGIN,
    LOGIN_WRONG,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    SET_DEFAULT_AUTH,
    REQUEST_LOGOUT,
    LOGOUT_SUCCESS,
    LOGOUT_ERROR
} from "../actionTypes/authActionTypes";

export function authReducer(
    state = {
        data: null,
        status: ApiStatus.STOP,
        error: null
    },
    action
) {
    switch (action.type) {
        case SET_DEFAULT_AUTH: {
            return {
                ...state,
                ...action.payload
            };
        }
        case REQUEST_LOGIN: {
            return Object.assign({}, state, {
                status: ApiStatus.START
            });
        }
        case LOGIN_SUCCESS: {
            return Object.assign({}, state, {
                status: ApiStatus.FINISH,
                data: action.payload
            });
        }
        case LOGIN_WRONG: {
            return Object.assign({}, state, {
                status: ApiStatus.FINISH,
                data: null
            });
        }
        case LOGIN_ERROR: {
            return Object.assign({}, state, {
                status: ApiStatus.ERROR,
                error: action.error
            });
        }
        case REQUEST_LOGOUT: {
            return { ...state, status: ApiStatus.START };
        }
        case LOGOUT_SUCCESS: {
            return { ...state, ...action.payload };
        }
        case LOGOUT_ERROR: {
            return { ...state, error: action.payload };
        }
        default:
            return state;
    }
}
