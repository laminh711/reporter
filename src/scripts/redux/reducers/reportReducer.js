import { SET_REPORT_LIST, ADD_REPORT } from "../actionTypes/reportActionTypes";

const defaultState = {
    reportList: [],
};

export function reportReducer(state = defaultState, action) {
    switch (action.type) {
        case SET_REPORT_LIST: {
            return {
                ...state,
                ...action.payload,
            };
        }
        case ADD_REPORT: {
            const newList = [...state.reportList, action.payload];
            return {
                ...state,
                reportList: newList,
            };
        }
        default:
            return state;
    }
}
