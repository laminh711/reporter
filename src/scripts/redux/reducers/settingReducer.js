import { SET_SETTINGS } from "../actions/settingActions";

const defaultState = {
    workDuration: 25 * 60,
    breakDuration: 5 * 60,
    longBreakDuration: 10 * 60,
    isNotificationOn: false,
    isSoundOn: true,
};
export function settingReducer(state = defaultState, action) {
    if (action.type === SET_SETTINGS) {
        return {
            ...state,
            ...action.payload,
        };
    }
    return state;
}
