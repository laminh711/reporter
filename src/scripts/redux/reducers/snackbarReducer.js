import { POP_MESSAGE, CLOSE_MESSAGE } from "../actionTypes/snackbarActionTypes";

const defaultState = {
    show: false,
    message: "",
};
export function snackbarReducer(state = defaultState, action) {
    if (action.type === POP_MESSAGE) {
        return {
            ...state,
            show: true,
            ...action.payload
        };
    }
    if (action.type === CLOSE_MESSAGE) {
        return {
            ...state,
            show: false,
        };
    }
    return state;
}
