import ApiStatus from "../../models/enums/ApiStatus";
import { REQUEST_WORKTIME, RECEIVE_WORKTIME } from "../actionTypes/worktimeActionTypes";
import { RECEIVE_ERROR } from "../actionTypes/errorActionTypes";

export function worktimeReducer(
    state = {
        data: null,
        status: ApiStatus.STOP,
        error: null
    },
    action
) {
    switch (action.type) {
        case REQUEST_WORKTIME: {
            return Object.assign({}, state, {
                status: ApiStatus.START
            });
        }
        case RECEIVE_WORKTIME: {
            return Object.assign({}, state, {
                status: ApiStatus.FINISH,
                data: action.payload
            });
        }
        case RECEIVE_ERROR: {
            return Object.assign({}, state, {
                status: ApiStatus.ERROR,
                error: action.error
            });
        }
        default:
            return state;
    }
}
