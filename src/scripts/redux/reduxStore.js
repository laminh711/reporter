import { applyMiddleware, createStore } from "redux";
import { createLogger } from "redux-logger";
import thunkMiddleware from "redux-thunk";
import ApiStatus from "../models/enums/ApiStatus";
import { setDefaultAuth } from "./actions/authActions";
import { isHollow } from "../utils/type_util";
import { fetchWorktime } from "./actions/worktimeActions";
import rootReducer from "./rootReducers";

const loggerMiddleware = createLogger();
export let reduxStore;
if (process.env.NODE_ENV === "development") {
    reduxStore = createStore(rootReducer, applyMiddleware(thunkMiddleware, loggerMiddleware));
} else {
    reduxStore = createStore(rootReducer, applyMiddleware(thunkMiddleware));
}

export function dispatchSetDefaultValue() {
    const existedAuthToken = localStorage.getItem("authToken");
    if (!isHollow(existedAuthToken)) {
        reduxStore.dispatch(
            setDefaultAuth({
                data: existedAuthToken,
                status: ApiStatus.FINISH,
                error: null,
            })
        );
        reduxStore.dispatch(fetchWorktime());
        return;
    } else {
        reduxStore.dispatch(
            setDefaultAuth({
                data: null,
                status: ApiStatus.STOP,
                error: null,
            })
        );
    }
}

export function getAppStore() {
    return reduxStore.getState().appReducer;
}

export function getAuthStore() {
    return reduxStore.getState().authReducer;
}
