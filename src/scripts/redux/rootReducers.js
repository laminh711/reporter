import { combineReducers } from "redux";
import { worktimeReducer } from "./reducers/worktimeReducer";
import { authReducer } from "./reducers/authReducer";
import { appReducer } from "./reducers/appReducer";
import { reportReducer } from "./reducers/reportReducer";
import { settingReducer } from "./reducers/settingReducer";
import { snackbarReducer } from "./reducers/snackbarReducer";

const rootReducers = combineReducers({
    appReducer,
    worktimeReducer,
    authReducer,
    reportReducer,
    settingReducer,
    snackbarReducer,
});

export default rootReducers;
