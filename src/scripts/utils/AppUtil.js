export function isDevEnv() {
    return process.env.NODE_ENV === "development";
}



export function envArr(key) {
    return process.env[key];
}
