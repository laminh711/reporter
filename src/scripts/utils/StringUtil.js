import { IsHollow } from "./TypeUtil";

export function IsEmpty(string) {
    return string === "" || IsHollow(string);
}
