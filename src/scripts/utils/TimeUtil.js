export const referenceZero = new Date(1996, 6, 11, 0, 0, 0, 0);

export function makeAlignedDate(d) {
    let date = d;
    date.setFullYear(referenceZero.getFullYear());
    date.setMonth(referenceZero.getMonth());
    date.setDate(referenceZero.getDate());
    return date;
}

export function totalSecondsToHMS(totalSeconds) {
    const minutes = Math.trunc(totalSeconds / 60) % 60;
    const hours = Math.trunc((totalSeconds / 3600) % 24);
    const seconds = totalSeconds % 60;
    return {
        minutes,
        seconds,
        hours,
    };
}