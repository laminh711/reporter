import fetch from "cross-fetch";
import { getAuthStore } from "../redux/reduxStore";

const base_url = "http://localhost:8754/api";
const get_worktimes_url = base_url + "/worktimes";
export class WorktimeAPI {
    static getWorkTime() {
        const basicAuthToken = getAuthStore();
        return fetch(get_worktimes_url, {
            method: "GET",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                Authorization: `Basic ${basicAuthToken.data}`
            },
            redirect: "follow",
            referrer: "no-referrer"
        })
            .then(response => {
                return response.json();
            })
            .then(json => {
                return json["data"][0];
            });
    }
}

const login_url = base_url + "/auth";
export class AuthAPI {
    static login(username, password) {
        return fetch(login_url, {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin",
            headers: {
                "Content-Type": "application/json"
            },
            redirect: "follow",
            referrer: "no-referrer",
            body: JSON.stringify({
                username,
                password
            })
        })
            .then(response => response.json())
            .then(json => json);
    }
}
