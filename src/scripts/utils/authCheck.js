import ApiStatus from "../models/enums/ApiStatus";

export class AuthCheck {
    static IsLogin(authStore) {
        const { data, status, error } = authStore;

        if (status === ApiStatus.FINISH) {
            if (error !== null) {
                return false;
            }
            if (data !== null) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
}
