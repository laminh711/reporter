export default class TimeUtil {
    static secondsToTime(totalSec) {
        let hours = Math.floor(totalSec / (60 * 60));
        let minutes = (Math.floor(totalSec / 60) % 3600) % 60;
        let seconds = (totalSec - minutes * 60) % 60;
        let obj = {
            hour: Math.trunc(hours),
            minute: Math.trunc(minutes),
            second: Math.trunc(seconds),
        };
        return obj;
    }

    static printDigital(totalSec) {
        const obj = this.secondsToTime(totalSec);
        return `${obj.hour}:${obj.minute}:${obj.second}`;
    }

    static getCurrentHour() {
        let now = new Date();
        return now.getHours();
    }
    static createDateWithCustomTime(minute, second, hour = this.getCurrentHour()) {
        let result = new Date();
        result.setHours(hour);
        result.setMinutes(minute);
        result.setSeconds(second);
        return result;
    }

    static createHourTime(h, m, s) {
        const randomYear = 1999;
        const randomMonth = 12;
        const randomDate = 12;
        return new Date(randomYear, randomMonth, randomDate, h, m, s);
    }

    static calculateDiff(dateA, dateB) {
        // return Math.abs((dateA.getTime() - dateB.getTime()) / 1000);
        return (dateA.getTime() - dateB.getTime()) / 1000;
    }

    static utcToZone(date) {
        const now = new Date();
        const diffInHour = now.getHours() - now.getUTCHours();
        const diffInMinute = now.getMinutes() - now.getUTCMinutes();

        date.setHours(date.getHours() - diffInHour);
        date.setMinutes(date.getMinutes() - diffInMinute);

        return date;
    }
}

export function makeHourTime(h, m, s) {
    const cy = 1999;
    const cm = 12;
    const cd = 12;
    return new Date(cy, cm, cd, h, m, s);
}

export const zeroHour = makeHourTime(0, 0, 0);

export function isZeroTime(d) {
    return d.getSeconds() === 0 && d.getMinutes() === 0 && d.getHours() === 0;
}

export function secondsToHourObject(totalSec) {
    let hours = Math.floor(totalSec / (60 * 60));
    let minutes = (Math.floor(totalSec / 60) % 3600) % 60;
    let seconds = (totalSec - minutes * 60) % 60;
    let obj = {
        hour: Math.trunc(hours),
        minute: Math.trunc(minutes),
        second: Math.trunc(seconds),
    };
    return obj;
}

export function toLocalDate(date) {
    const tz = Intl.DateTimeFormat().resolvedOptions().timeZone;

    var localTime = date.toLocaleString("en-US", { timeZone: tz });
    localTime = new Date(localTime);

    return localTime;
}
