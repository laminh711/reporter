export function isHollow(suspect) {
    return typeof suspect === "undefined" || suspect === null || suspect === "";
}
